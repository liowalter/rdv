/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {RawEditorSettings} from "tinymce";

export interface HeaderSettings {
  [key: string]: HeaderSettingsModel
}

export interface FooterSettings {
  [key: string]: FooterSettingsModel
}

export interface IiifViewerMobileConfig {
  maxWidth: number,
  url: string
}

export interface ViewOrder {
  view: Page;
  /**
   * The lower the number is, the higher a viewer
   * is displayed in the viewer selection dropdown.
   * Default: 50
   */
  order: number;
}

export type BackendViewerType = "NONE" | "ALL" | string[];

export interface DocumentViewer {
  /**
   * Use this viewer.
   */
  readonly enabled: boolean;

  /**
   * The document viewer shall be used on the specified pages.
   * If absent, the viewer is suitable for all pages.
   */
  readonly enabledForPage?: ViewOrder[];

  /**
   * Type of viewer.
   */
  readonly type: ViewerType;

  /**
   * Internal link to viewer.
   */
  readonly viewerUrl: string;

  /**
   * Internal link to icon.
   */
  readonly iconUrl: string;

  /**
   * URL which returns document viewers's config.
   */
  readonly viewerConfigUrl?: string;

  /**
   * URL which returns document viewers's config for a special width.
   * The closest element is chosen. If none is found "viewerConfigUrl" is used.
   * E.g. with
   *   [{maxWidth: 200, url: "u1"},{maxWidth: 400, url: "u2"}]
   * and a viewport width of 100 and 200, "u1" is used. For 201 "u2". For 400
   * "viewerConfigUrl".
   */
  readonly viewerConfigMobilUrl?: IiifViewerMobileConfig[];

  /**
   * When switching to IIIF result view only the listed facet values are preserved.
   * All other selected values of this facet are automatically removed.
   * If no selected value remains, the first facet value of the list is automatically selected.
   * If messageTextKey: is specified, a localized toast message is displayed
   * (with optional messageTitleKey:).
   */
  readonly facetRestrictions?: IIIFRestriction[];

  /**
   * Mirador accepts the following values: "share", "download","image-tools","text-overlay"
   * Universal Viewer has currently no plugins.
   */
  readonly disabledPlugins?: string[];
}

export interface BaseSettings {
  /**
   * Two letter value for default language. If missing "de" is used.
   */
  readonly defaultLanguage?: string;

  /**
   * Optional overlay for translation files in src/assets/i18n/.
   * Use "de", "en" for the outer key and any i18n key for the inner key
   * to overwrite specific translations.
   */
  readonly i18n?: { [key: string]: { [key: string]: string } };

  /**
   * Used in production
   */
  readonly production: boolean;

  /**
   * URL of proxy script
   */
  readonly proxyUrl: string;

  /**
   * URL of suggestion proxy script
   */
  readonly suggestSearchWordProxyUrl?: string;

  /**
   * URL of more proxy script
   */
  readonly moreProxyUrl?: string;

  /**
   * Used to get popup info with normal search results. Only used
   * for external links.
   */
  readonly popupQueryProxyUrl?: string;

  /**
   * URL of detail proxy script
   */
  readonly detailProxyUrl?: string;

  /**
   * URL of edit detail proxy script
   */
  readonly detailEditProxyUrl?: string;

  /**
   * URL of auto-complete detail field proxy script
   */
  readonly detailSuggestionProxyUrl?: string;

  /**
   * URL of next detail proxy script
   */
  readonly navDetailProxyUrl?: string;

  /**
   * Used if result list is displayed in IIIF Universal viewer.
   */
  readonly documentViewerProxyUrl?: string;

  /**
   * In-facet search proxy endpoint.
   */
  readonly inFacetSearchProxyUrl?: string;

  /**
   * Documents are only editable if this option is true.
   */
  readonly editable?: boolean;

  /**
   * Whether to let IIIF scale a doc's preview image (true, default) or use CSS (false).
   */
  readonly scaleDocThumbsByIiif?: boolean;
}

/**
 * To be implemented by settings (environment) files
 */
export interface SettingsModel extends BaseSettings {
  /**
   * Display preview images of documents.
   * Images are displayed if this field is absent or true.
   */
  readonly showImagePreview?: boolean;

  /**
   * Display the simple single field search.
   * Default: true
   */
  readonly showSimpleSearch?: boolean;

  /**
   * If true, doc viewer uses whole screen width.
   */
  readonly  viewerExtraWide?: boolean;

  /**
   * If true, display document's field and iiif viewer side-by-side.
   * This enables a full width mode too.
   * Otherwise either document fields or iiif viewer.
   */
  readonly embeddedIiifViewer?: false | 'left' | 'right';

  /**
   * If true, display document's field ans iiif viewer stacked.
   * Otherwise either document fields or iiif viewer.
   */
  readonly narrowEmbeddedIiiFViewer?: false | 'top' | 'bottom';

  /**
   * Backend store
   */
  readonly backend: Backend;

  /**
   * Base URL (used as prefix for links)
   */
  readonly baseUrl: string;

  /**
   * One key of "documentViewer" or "List" (default).
   */
  readonly defaultDocumentViewerName?: string;

  /**
   * A list of all installed document viewer like UV, mirador etc.
   * Note: The internal viewer "List" can't be configured.
   */
  readonly documentViewer?: { [key: string]: DocumentViewer };

  /**
   * About link to external system.
   * If an object is specified, the key is the language. A value for
   * the "defaultLanguage" is required.
   */
  readonly externalAboutUrl?: string | { [key: string]: string };

  /**
   * Help link to external system.
   * If an object is specified, the key is the language. A value for
   * the "defaultLanguage" is required.
   */
  readonly externalHelpUrl?: string | { [key: string]: string };

  /**
   * Search field configurations
   */
  readonly searchFields: SearchFieldsModel;

  /**
   * Available sort options.
   */
  readonly sortFields?: SortFieldsModel[];

  /**
   * Filter configurations
   */
  readonly filterFields: FilterFieldsModel;

  /**
   * Facet configurations
   */
  readonly facetFields: FacetFieldsModel;

  /**
   * Range configurations
   * @Deprecated put these facets into facetFields too
   */
  readonly rangeFields: RangeFieldsModel;

  /**
   * Selectable element per page options
   */
  readonly rowOpts: number[];

  /**
   * Result list display options
   */
  readonly queryParams: QueryParamsModel;

  /**
   * Basket element list display options
   */
  readonly basketConfig: {
    readonly queryParams: QueryParamsModel,
  };

  /**
   * Show export list button
   */
  readonly showExportList: {
    /**
     * In basket element list
     */
    readonly basket: boolean,
    /**
     * In result list
     */
    readonly table: boolean,
  };

  /**
   *
   */
  readonly tableFields: TableColumnModel[];
  readonly extraInfos: ExtraInfosModel;

  /**
   * Customize header area.
   * "key" is either: "default", "de", "en", ...
   * If the whole language entry or a single field of a language doesn't exist,
   * the value is taken from "default".
   */
  readonly headerSettings?: HeaderSettings;

  /**
   * Customize footer area (optional).
   * "key" is either: "default", "de", "en", ...
   * If the whole language entry or a single field of a language doesn't exist,
   * the value is taken from "default".
   */
  readonly footerSettings?: FooterSettings;

  /**
   * Customize tinymce.
   * See https://www.tiny.cloud/docs/configure/
   */
  readonly richtextEditor?: RawEditorSettings;
}

export function headerSettingsModelByLanguage(languageCode: string, headerSettings: HeaderSettings): HeaderSettingsModel {
  let model: HeaderSettingsModel = undefined;
  if (headerSettings) {
    if (headerSettings[languageCode]) {
      model = {...headerSettings.default, ...headerSettings[languageCode]};
    } else {
      model = {...headerSettings.default};
    }
  }
  return model;
}

export function footerSettingsModelByLanguage(languageCode: string, footerSettings: FooterSettings): FooterSettingsModel {
  let model: FooterSettingsModel = undefined;
  if (footerSettings) {
    if (footerSettings[languageCode]) {
      model = {...footerSettings.default, ...footerSettings[languageCode]};
    } else {
      model = {...footerSettings.default};
    }
  }
  return model;
}

export interface IIIFRestriction {
  field: string;
  messageTitleKey?: string;
  messageTextKey: string;
  values: any[];
}

export enum ViewerType {
  'UV' = 'UV',
  'MIRADOR' = 'Mirador'
}

export enum Page {
  'Detail' = 'Detail',
  'Search' = 'Search'
}

export enum ListType {
  'LIST' = 'List'
}

export type ResultView = ViewerType | ListType;

/**
 * Used backend
 */
export enum Backend {
  'ELASTIC' = 'elastic',
  'SOLR' = 'solr'
}

/**
 * Sort order of selected row in list
 */
export enum SortOrder {
  'ASC' = 'asc',
  'DESC' = 'desc',
}

/**
 * Type of extra information field. If set to `LINK`, a link is created out of the text
 */
export enum ExtraInfoFieldType {
  'LINK' = 'link',
  'TEXT' = 'text',
}

export enum FacetFieldType {
  'SIMPLE' = 'simple',
  'RANGE' = 'range',
  'HISTOGRAM' = 'histogram',
  'HIERARCHIC' = 'hierarchic',
  'SUBCATEGORY' = 'subcat',
}

export enum HistogramFieldType {
  DATE = 'date',
  INT = 'int',
  GEO = 'geo'
}

export enum FacetValueOrder {
  COUNT = 'count',
  LABEL = 'label',
}

/**
 * Queryable search fields
 */
interface SearchFieldsModel {
  /**
   * List elements. Key is sent to backend, while value is shown in UI
   */
  readonly options: any;
  /**
   * Preselected element
   */
  readonly preselect: string[];
}

/**
 * Available sort option.
 */
export interface SortFieldsModel {
  /**
   * A field.
   */
  field: string;
  /**
   * Sort direction.
   */
  order: SortOrder;
  /**
   * Label displayed in UI.
   */
  display: string;
}

/**
 * Search filter configuration
 */
interface FilterFieldsModel {
  [index: string]: {
    /**
     * Field name
     */
    readonly field: string;
    /**
     * Displayed label
     */
    readonly label: string;
    /**
     * Url used for remotely fetching filter options
     */
    readonly url?: string;
    /**
     * Available options. Empty if options are fetched remotely.
     */
    readonly options: {
      /**
       * Displayed label
       */
      readonly label: string;
      /**
       * Option value
       */
      readonly value: string;
    }[];
  }
}

export interface CommonFacetFieldModel {
  /**
   * Facet's name
   */
  readonly field: string;

  /**
   * Displayed label
   */
  readonly label: string;

  /**
   * List order relative to other facet AND range categories
   */
  readonly order: number;

  /**
   * Number of facet values to show if "more" button is clicked.
   */
  readonly expandAmount?: number;

  /**
   * Type of facet.
   */
  readonly facetType?: FacetFieldType;
  /**
   * Passed to request to define how many entries shall be returned from backen on first call.
   */
  readonly size?: number;

  /**
   * Is search in facet values wanted?
   */
  readonly searchWithin?: boolean;

  /**
   * Is the facet visible in the UI?
   */
  readonly hidden?: boolean;

  /**
   * On first app access open this facet or not (default: closed).
   */
  readonly initiallyOpen?: boolean;
}

export interface FacetFieldModel extends CommonFacetFieldModel {
  /**
   * Default operator for combining individual facet elements
   */
  readonly operator: string;

  /**
   * List of all available operators
   */
  readonly operators?: string[];

  /**
   * Optional i18n key for a tooltip text on a facet.
   */
  readonly help?: string;

  /**
   * Use this facet for search term suggestions?
   * If absent or 0, no. Otherwise yes.
   * Note: This attribute is only applicable to simple facets.
   */
  autocomplete_size?: number;

  /**
   * The default valueOrder. If missing "count" is used.
   */
  readonly valueOrder?: FacetValueOrder;

  /**
   * All available facet values orders.
   */
  readonly valueOrders?: FacetValueOrder[];
}

export interface HistogramFieldModel extends FacetFieldModel {
  /**
   * Type of histogram.
   */
  data_type: HistogramFieldType;

  /**
   * Aggregation
   */
  showAggs: boolean;
}

export interface RangeFieldModel extends CommonFacetFieldModel {
  /**
   * First originally displayed value
   */
  readonly from: any;
  /**
   * Last possible value
   */
  readonly max: any;
  /**
   * First possible value
   */
  readonly min: any;
  /**
   * Show results who lacks respective values nevertheless
   */
  readonly showMissingValues: boolean;
  /**
   * Last originally displayed value
   */
  readonly to: any;
}

/**
 * Facet categories configuration
 */
export interface FacetFieldsModel {
  [index: string]: FacetFieldModel;
}

/**
 * Range categories configuration
 */
export interface RangeFieldsModel {
  [index: string]: RangeFieldModel
}

/**
 * Result list configuration
 */
export interface QueryParamsModel {
  /**
   * Number of elements / rows per page
   */
  readonly rows: number;
  /**
   * Original offset (0 if not set explicitly)
   */
  readonly offset?: number;
  /**
   * Sort by field
   */
  readonly sortField: string;
  /**
   * Sort order
   */
  readonly sortOrder: SortOrder
  /**
   * Display search result in document viewer or 'List'.
   */
  readonly usedDocumentViewer?: string;
}

/**
 * Configuration for table column
 */
export interface TableColumnModel {
  /**
   * Additional css classes used in fields
   */
  readonly css: string;
  /**
   * Enable button to show additional information (per field; disabled per default)
   */
  readonly extraInfo?: boolean;
  /**
   * Column name (equal to field name in backend)
   */
  readonly field: string;
  /**
   * Displayed label of column
   */
  readonly label: string;
  /**
   * Link to detailed view (per field; disabled per default)
   */
  readonly landingpage?: boolean;
  /**
   * Column name used for sorting
   */
  readonly sort: string;

  /**
   * Flag to indicate that this column contains the name of a field.
   */
  readonly isTitle?: boolean;
}

/**
 * Field's additional information configuration (information shown when extra
 * information drop-down is activated)
 */
interface ExtraInfosModel {
  [index: string]: {
    /**
     * Content type of field
     */
    readonly display: ExtraInfoFieldType;
    /**
     * Field's name
     */
    readonly field: string;
    /**
     * Displayed label
     */
    readonly label: string;
  }
}

export interface HeaderSettingsModel {
  /**
   * Name of the web app
   */
  readonly name?: string;
  // disable whole header if true otherwise the header is displayed
  disable?: boolean;
  // display portal name in separate area
  showPortalName?: boolean;
  // use this department name instead of the portal name
  useLogoSubTitle?: boolean;
  // display this logo at the right side of the header
  useDepartmentLogoUrl?: boolean;
  // disable whole language bar if true otherwise the bar is displayed
  disableLanguageBar?: boolean;
  // display FR language button in header
  showFrenchLanguageSwitch?: boolean;
  // display IT language button in header
  showItalianLanguageSwitch?: boolean;
  // displays red beta bar on top of UI
  // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
  showBetaBar?: boolean;
}

export interface FooterSettingsModel {
  // disable whole footer if true, otherwise the footer is displayed
  disable?: boolean;
  // display to-top-arrow if undefined or true
  displayToTop?: boolean;
  // display "Universität Basel"?
  displayHostUrl?: boolean;
  // i18n keys of host link url/name (if displayHostUrl is true)
  hostUrl?: string;
  hostName?: string;
  // display portal URL?
  displayPortalUrl?: boolean;
}
