/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {inject, TestBed} from '@angular/core/testing';
import {Injectable} from '@angular/core';

import {FacetValueProcessor, SearchActions, SearchValueProcessor, UrlParamNames, UrlParamsService} from './url-params.service';
import {Store, StoreModule} from "@ngrx/store";

import * as fromSearch from '../../search/reducers';
import {FacetValueOrder, SortOrder} from "@app/shared/models/settings.model";
import {ReplaceUrlParams, ViewerValueProcessor} from "@app/shared/services/url-params.service";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";


class TestFacetProc implements FacetValueProcessor {
  collectedInfos = {facets: [], histogram: [], hierarhic: [], operators: [], facetValueOrders: []};

  processSimpleFacet?(field: string, value: string): SearchActions[] {
    this.collectedInfos.facets.push({field, value});
    return [];
  }

  processHistogramFacet?(field: string, from: string, to: string): SearchActions[] {
    this.collectedInfos.histogram.push({field, from, to});
    return [];
  }

  processHierarchicFacet?(field: string, label: string, values: string[]): SearchActions[] {
    this.collectedInfos.hierarhic.push({field, values});
    return [];
  }

  processFacetOperator?(field: string, operator: string): SearchActions[] {
    this.collectedInfos.operators.push({field, operator});
    return [];
  }

  processFacetValueOrder?(field: string, order: string): SearchActions[] {
    this.collectedInfos.facetValueOrders.push({field, order});
    return [];
  }
}

class TestSearchProc implements SearchValueProcessor {
  collectedInfos = {searchText: [], sorting: []};

  processSimpleSearchValue(searchText: string[], searchField: string): SearchActions[] {
    this.collectedInfos.searchText.push({searchText, searchField});
    return [];
  }

  processSorting(sortField: string, sortDir: SortOrder): SearchActions[] {
    this.collectedInfos.searchText.push({sortField, sortDir});
    return [];
  }

}

class TestViewerProc implements ViewerValueProcessor {
  collectedInfos = {viewer: []};

  processViewer(name: string): SearchActions[] {
    this.collectedInfos.viewer.push({name});
    return [];
  }

}

@Injectable()
export class StoreMock<T = any> extends Store<T> {

  public source = new BehaviorSubject<any>({});

  public overrideState(allStates: any) {
    this.source.next(allStates);
  }

}

describe('UrlParamService', () => {
  let store: StoreMock<fromSearch.State>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({
        ...fromSearch.reducers,
      })],
      providers: [UrlParamsService, {provide: Store, useClass: StoreMock}]
    });
    store = TestBed.inject(Store) as StoreMock<fromSearch.State>;
    store.overrideState({
      search: {
        form: {
          searchFields: {
            search0: {
              field: "all",
              value: []
            }
          },
          facetFields: {},
          histogramFields: {},
          hierarchyFields: {}
        },
        query: {
          sortField: "",
          sortDir: ""
        }
      }
    });
  });

  it('should be created', inject([UrlParamsService], (service: UrlParamsService) => {
    expect(service).toBeTruthy();
    const queryParams = "facets=Quellname%7ELe+Temps%21Der+Bund%0A";
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    // don't use decodeURIComponent(), because "+" aren't resolved!
    service.parseFacet(up.get("facets"), proc);
    expect(proc.collectedInfos.facets.length).toEqual(2);
    expect(proc.collectedInfos.facets[0]).toEqual({field: 'Quellname', value: 'Le Temps'});
    expect(proc.collectedInfos.facets[1]).toEqual({field: 'Quellname', value: 'Der Bund\x0a'});
  }));

  it('should un-/escape',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();

        // url encoding - decoding round trip
        const replaceUrls = new ReplaceUrlParams(store, null);
        // "!" is the search text separator in the URL and needs to be escaped
        const searchedText = ["A!B!", "C!D!"];

        // encode ...
        replaceUrls.setSearch(searchedText, "all");
        const params = replaceUrls.toUrlParams();
        const searchTextParam = params[UrlParamNames.SEARCHED_TEXT];
        expect(searchTextParam).toContain(searchedText[0].replace(/!/g, "\\!"));
        expect(searchTextParam).toContain(searchedText[1].replace(/!/g, "\\!"));
        const searchFieldParam = params[UrlParamNames.SEARCHED_FIELD];

        // decode ...
        const queryParams = UrlParamNames.SEARCHED_TEXT + "=" + searchTextParam
          + "&" + UrlParamNames.SEARCHED_FIELD + "=" + searchFieldParam;
        const up = new URLSearchParams(queryParams);
        const proc: TestSearchProc = new TestSearchProc();
        service.parseSearch(up.get(UrlParamNames.SEARCHED_TEXT), up.get(UrlParamNames.SEARCHED_FIELD), {all: true}, proc);
        expect(proc.collectedInfos.searchText.length).toEqual(1);
        expect(proc.collectedInfos.searchText[0].searchText.length).toEqual(2);
        expect(proc.collectedInfos.searchText[0].searchText).toContain(searchedText[0]);
        expect(proc.collectedInfos.searchText[0].searchText).toContain(searchedText[1]);
      }));

  it('should process viewer',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const selectedViewer = "Mirador"; // has to be defined in env!

        // encode
        replaceUrls.setViewer(selectedViewer);
        const params = replaceUrls.toUrlParams();
        const viewerParam = params[UrlParamNames.VIEWER];
        expect(viewerParam).toEqual(selectedViewer);

        // decode
        const queryParams = UrlParamNames.VIEWER + "=" + selectedViewer;
        const up = new URLSearchParams(queryParams);
        const proc: TestViewerProc = new TestViewerProc();
        service.parseViewer(up.get(UrlParamNames.VIEWER), proc);
        expect(proc.collectedInfos.viewer.length).toEqual(1);
        expect(proc.collectedInfos.viewer[0].name).toEqual(selectedViewer);
      }));

  function opChecker(replaceUrls: ReplaceUrlParams,
                     service: UrlParamsService,
                     searchedFacet: string,
                     searchedOp: string) {

    // encode
    const params = replaceUrls.toUrlParams();
    const searchParam = params[UrlParamNames.OPS];
    expect(searchParam).toBeDefined("Found no ops");
    expect(searchParam.length).toEqual(1, "Expected 1 op in url");
    expect(searchParam[0]).toContain(searchedFacet);
    expect(searchParam[0]).toContain(searchedOp);

    // decode
    const queryParams = UrlParamNames.OPS + "="
      + searchedFacet + ReplaceUrlParams.SEPARATOR_FACET_OPERATOR + searchedOp;
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    service.parseOperator(up.get(UrlParamNames.OPS), proc);
    expect(proc.collectedInfos.operators.length).toEqual(1);
    expect(proc.collectedInfos.operators[0].field).toEqual(searchedFacet);
    expect(proc.collectedInfos.operators[0].operator).toEqual(searchedOp);
  }

  it('should process simple facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "OR";

        replaceUrls.setFacetOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  it('should process hierarchic facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "AND";

        replaceUrls.setHierarchicOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  it('should process histogram facet operator',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOp = "NOT";

        replaceUrls.setHistogramOperator(searchedFacet, searchedOp);
        opChecker(replaceUrls, service, searchedFacet, searchedOp);
      }));

  function facetValueOrderChecker(replaceUrls: ReplaceUrlParams,
                                  service: UrlParamsService,
                                  searchedFacet: string,
                                  searchedOrder: string) {
    // encode
    const params = replaceUrls.toUrlParams();
    const searchParam = params[UrlParamNames.FACET_VALUE_ORDER];
    expect(searchParam.length).toEqual(1);
    expect(searchParam[0]).toContain(searchedFacet);
    expect(searchParam[0]).toContain(searchedOrder);

    // decode
    const queryParams = UrlParamNames.FACET_VALUE_ORDER + "="
      + searchedFacet + ReplaceUrlParams.SEPARATOR_FACET_VALUE_ORDER + searchedOrder;
    const up = new URLSearchParams(queryParams);
    const proc: TestFacetProc = new TestFacetProc();
    service.parseFacetValueOrder(up.get(UrlParamNames.FACET_VALUE_ORDER), proc);
    expect(proc.collectedInfos.facetValueOrders.length).toEqual(1);
    expect(proc.collectedInfos.facetValueOrders[0].field).toEqual(searchedFacet);
    expect(proc.collectedInfos.facetValueOrders[0].order).toEqual(searchedOrder);
  }

  it('should process simple facet value order',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOrder = FacetValueOrder.LABEL;

        replaceUrls.setFacetValueOrder(searchedFacet, searchedOrder);
        facetValueOrderChecker(replaceUrls, service, searchedFacet, searchedOrder);
      }));

  it('should process hierarchic facet value order',
    inject([UrlParamsService],
      (service: UrlParamsService) => {
        expect(service).toBeTruthy();
        const replaceUrls = new ReplaceUrlParams(store, null);
        const searchedFacet = "Quellname";
        const searchedOrder = FacetValueOrder.COUNT;

        replaceUrls.setHierarchicFacetValueOrder(searchedFacet, searchedOrder);
        facetValueOrderChecker(replaceUrls, service, searchedFacet, searchedOrder);
      }));
});
