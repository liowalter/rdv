import {Injectable} from "@angular/core";
import * as fromQueryActions from "@app/search/actions/query.actions";
import {I18nToastrService} from "@app/shared/services/i18n-toastr.service";
import * as fromSearch from "@app/search/reducers";
import {LocalizeRouterService} from "@gilsdav/ngx-translate-router";
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ResetUtilService {

  constructor(protected _searchStore: Store<fromSearch.State>,
              protected localize: LocalizeRouterService,
              protected router: Router,
              protected toastr: I18nToastrService) {
  }

  public scrollToTop(): void {
    window.scrollTo({top: 0, left: 0, behavior: "smooth"});
  }

  protected finalAction(scrollToTop?: boolean) {
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
    if (scrollToTop) {
      this.scrollToTop();
    }
  }

  public switchToSearchView(): Promise<boolean> {
    return this.router.navigateByUrl(this.getSearchUrl());
  }

  public getSearchUrl() {
    return this.localize.translateRoute("/") as string;
  }

  public reload(scrollToTop?: boolean) {
    this.toastr.clear();
    this._searchStore.dispatch(new fromQueryActions.Reset());
    this._searchStore.dispatch(new fromQueryActions.ResetQuery()); // resets sorting, docs per page, doc view mode
    // can't use ctor dependency injection, because it would be done only once
    const currentPath = window.location.pathname;
    if (currentPath === this.getSearchUrl()) {
      setTimeout(() => this.finalAction(scrollToTop), 100);
    } else {
      this.switchToSearchView().then(() => {
        setTimeout(() => this.finalAction(scrollToTop), 100);
      });
    }
  }
}
