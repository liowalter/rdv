/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/* tslint:disable:no-bitwise */

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item: any): boolean {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

/**
 * Deep merge two objects.
 * @param target
 * @param sources
 */
export function mergeDeep(target: any, ...sources: any[]): any {
  if (!sources.length) {
    return target;
  }
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (source.hasOwnProperty(key) && isObject(source[key])) {
        if (!target[key]) {
          Object.assign(target, {[key]: {}})
        }
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, {[key]: source[key]});
      }
    }
  }

  return mergeDeep(target, ...sources);
}

/**
 * Create a deep clone of a json object.
 * Not suitable to copy class instances!
 *
 * @param source
 * @returns {any}
 */
export function deepJsonClone(source: any): any {
  if (source !== null && typeof source === 'object' && !Array.isArray(source)) {
    const results = {};
    for (const key in source) {
      if (source.hasOwnProperty(key)) {
        results[key] = deepJsonClone(source[key]);
      }
    }
    return results;
  } else if (typeof source === 'object' && Array.isArray(source)) {
    const results = [];
    for (let i = 0; i < source.length; i++) {
      {
        results[i] = deepJsonClone(source[i]);
      }
    }
    return results;
  } else {
    return source;
  }
}

/**
 * Caches result of a function with a given argument
 * @param fn
 */
export const memoize = (fn) => {
  const cache = {};
  return (x) => {
    if (x in cache) {
      return cache[x];
    }
    return cache[x] = fn(x);
  };
};

/**
 * Generates random hash code
 */
export function randomHashCode() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

/**
 * Generates random hash from string
 * @param s {string}
 */
export function hashCodeFromString(s: String) {
  let hash = 0, i, chr;
  if (s.length === 0) {
    return hash;
  }
  for (i = 0; i < s.length; i++) {
    chr = s.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
}

export const stringToEnumValue = <ET, T>(enumObj: ET, str: string): T =>
  (enumObj as any)[Object.keys(enumObj).filter(k => (enumObj as any)[k] === str)[0]];

export function empty(obj: any): boolean {
  if (obj === null || obj === undefined) {
    return true;
  }
  if (obj.constructor === Array) {
    return obj.length === 0;
  }
  return ('' + obj).trim().length === 0;
}
