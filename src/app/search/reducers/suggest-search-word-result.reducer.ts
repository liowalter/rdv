/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {SuggestSearchWordResultActions, SuggestSearchWordResultActionTypes} from '../actions/suggest-search-word.actions';
import {AutoCompleteValue} from "@app/shared/models/doc-details.model";


export type State = EntityState<AutoCompleteValue>;

/**
 * @ignore
 */
export const adapter: EntityAdapter<AutoCompleteValue> = createEntityAdapter<AutoCompleteValue>();

/**
 * @ignore
 */
export const initialState: State = adapter.getInitialState({});

/**
 * @ignore
 */
export function reducer(
  state = initialState,
  action: SuggestSearchWordResultActions
): State {
  switch (action.type) {
    case SuggestSearchWordResultActionTypes.SetResults: {
      return adapter.addMany(action.payload.results, adapter.removeAll(state));
    }

    default: {
      return state;
    }
  }
}

/**
 * @ignore
 */
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
