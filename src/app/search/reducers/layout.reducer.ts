/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import * as fromActions from '../actions/layout.actions';
import {environment} from '../../../environments/environment';


/**
 * Layout state in search view
 */
export interface State {
  /**
   * Name (object key) of displayed facet or range (prefixed with `facet-pills-`)
   */
  shownFacetOrRange: string;
  shownMap: boolean;
  shownTimeline: boolean;
}

/**
 * @ignore
 */
const facetsAndRanges = {...environment.rangeFields, ...environment.facetFields};

/**
 * @ignore
 */
export const initialState: State = {
  shownFacetOrRange: 'facet-pills-' + Object.keys(facetsAndRanges).filter(k => facetsAndRanges[k].order === 1)[0],
  shownMap: false,
  shownTimeline: false,
};

/**
 * @ignore
 */
export function reducer(state = initialState, action: fromActions.LayoutActions): State {
  switch (action.type) {

    case fromActions.LayoutActionTypes.ShowFacetOrRange:
      return {
        ...state,
        shownFacetOrRange: action.payload,
      };

    case fromActions.LayoutActionTypes.ToggleMapDisplay:
      return {
        ...state,
        shownMap: !state.shownMap,
      };

    case fromActions.LayoutActionTypes.ToggleTimelineDisplay:
      return {
        ...state,
        shownTimeline: !state.shownTimeline,
      };

    default:
      return state;
  }
}
