/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {environment} from "@env/environment";
import {TranslateService} from "@ngx-translate/core";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {debounceTime, skip, take} from "rxjs/operators";
import {IiifViewerMobileConfig, ListType, ViewerType} from "@app/shared/models/settings.model";
import {BreakpointObserver} from "@angular/cdk/layout";

// window.onmessage = function (e) {
//   console.log("FROM IFRAME", e.data, e);
// };

/**
 * Embed Mirador 3.X.
 */
@Component({
  selector: 'app-mirador',
  template: `
    <div class="detailed__universalviewer-container">
      <iframe #mirador class="detailed__universalviewer" [src]="frameSrc | safe" allowfullscreen></iframe>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MiradorComponent implements OnDestroy, AfterViewInit, OnChanges {
  @ViewChild("mirador") iframeRef: ElementRef<HTMLIFrameElement>;
  @Input() manifestUrl: string;
  @Input() version: string;

  languageMap = {"en": "en", "de": "de", "it": "it", "fr": "fr"};

  frameSrc: string;

  languageChangeSubscription: Subscription;

  searchFields$: Observable<any>;
  searchFieldByKey$: Observable<any>;

  breakPointSubscription: Subscription;
  breakPoints: number[];
  sortedBreakPoints: IiifViewerMobileConfig[];

  /**
   * @ignore
   */
  constructor(protected searchStore: Store<fromSearch.State>,
              protected translate: TranslateService,
              protected breakpointObserver: BreakpointObserver) {
    this.frameSrc = "";
    this.sortedBreakPoints = [];
    this.breakPoints = [];
    this.searchFields$ = searchStore.pipe(select(fromSearch.getSearchValues));
    this.searchFieldByKey$ = searchStore.pipe(select(fromSearch.getSearchValuesByKey));
  }

  protected setMiradorUrl() {
    if (this.manifestUrl) {
      let locale = "en";
      if (this.languageMap[this.translate.currentLang]) {
        locale = this.languageMap[this.translate.currentLang];
      }

      const config = environment.documentViewer[this.version];

      let baseUvUrl = config.viewerUrl;
      if (!baseUvUrl) {
        baseUvUrl = "/assets/mirador/init.html";
      }
      const paramSeparator = (baseUvUrl.indexOf("?") < 0) ? "?" : "&";
      let uvUrl = baseUvUrl + paramSeparator + "c=" + (new Date()).getTime() +
        "&manifest=" + encodeURIComponent(this.manifestUrl) +
        "&locale=" + locale;
      const url = this.getConfigByWidth();
      if (url) {
        uvUrl += "&config=" + encodeURIComponent(url);
      }
      if (config.disabledPlugins) {
        uvUrl += "&dp=" + config.disabledPlugins.map(s => encodeURIComponent(s)).join(",");
      }

      let firstSearchField;
      this.searchFields$.pipe(take(1)).subscribe((fields) => firstSearchField = Object.keys(fields)[0]);
      let searchedValue;
      this.searchFieldByKey$.pipe(take(1))
        .subscribe((field) => searchedValue = field(firstSearchField).value.join(" "));
      if (searchedValue) {
        uvUrl += "&h=" + encodeURIComponent(searchedValue);
      }

      // avoid iframe's url in browser history
      if (this.iframeRef) {
        const iframe = this.iframeRef.nativeElement;
        const iframeParent = iframe.parentElement;
        iframe.remove();
        iframe.src = uvUrl;
        iframeParent.append(iframe);
      } else {
        this.frameSrc = uvUrl;
      }
    }
  }

  protected languageChanged() {
    this.setMiradorUrl();
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.languageChangeSubscription = this.translate.onLangChange.subscribe(() => this.languageChanged());
  }

  getConfigByWidth(): string {
    let url;
    for (let i = 0; i < this.sortedBreakPoints.length; i++) {
      const bp = this.sortedBreakPoints[i];
      if (this.breakpointObserver.isMatched("(max-width: " + bp.maxWidth + "px)")) {
        url = bp.url;
        break;
      }
    }
    if (!url) {
      const config = environment.documentViewer[this.version];
      url = config.viewerConfigUrl;
    }
    return url;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.breakPointSubscription || changes["version"]) {
      // use breakpoint definitions from env
      let version = this.version;
      if (changes["version"]) {
        version = changes["version"].currentValue;
      }
      this.sortedBreakPoints = [];
      this.breakPoints = [];
      const config = environment.documentViewer[version];
      if (config.viewerConfigMobilUrl) {
        this.sortedBreakPoints = config.viewerConfigMobilUrl.sort((bp1, bp2) => bp1.maxWidth - bp2.maxWidth);
        this.breakPoints = this.sortedBreakPoints.map((bp) => bp.maxWidth).sort();
      }

      const BPS = this.breakPoints.map(w => "(max-width: " + w + "px)");
      let defaultBp = 0;
      if (this.breakPoints && this.breakPoints.length > 0) {
        defaultBp = this.breakPoints[this.breakPoints.length - 1] + 1;
      }
      const BP_NORMAL = "(min-width: " + defaultBp + "px)";
      BPS.push(BP_NORMAL);
      if (this.breakPointSubscription) {
        this.breakPointSubscription.unsubscribe();
      }
      // skip initial value, because setMiradorUrl() is called below
      this.breakPointSubscription = this.breakpointObserver.observe(BPS)
        .pipe(skip(1), debounceTime(250))
        .subscribe(result => {
          if (result.matches) {
            this.setMiradorUrl();
          }
        });
    }

    this.setMiradorUrl();
  }

}
