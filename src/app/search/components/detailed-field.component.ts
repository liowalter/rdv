/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {
  DocFieldEdit,
  DocFieldValue,
  FacetSelection,
  FieldCase,
  FieldType,
  LocalizedLabel,
  LocalizedValue,
  ValueCase
} from "@app/shared/models/doc-details.model";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "@env/environment";
import {DateFormatService} from "@app/shared/services/date-format.service";
import * as fromFormActions from "@app/search/actions/form.actions";
import {Store} from "@ngrx/store";
import * as fromSearch from "@app/search/reducers";
import {ResetUtilService} from "@app/shared/services/reset-util.service";
import * as fromQueryActions from "@app/search/actions/query.actions";
import {defaultViewer} from "@app/shared/models/settings-util";


/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-detailed-field',
  template: `
    <ng-template #simpleValueTemplate let-value="value">
      <ng-container [ngSwitch]="calculateValueView(meta.data_type)">
        <ng-container *ngSwitchCase="ValueCaseInTpl.DATE">{{formatDay(value)}}</ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.STRING_SHORT"><span [innerHTML]="value | safeHtml"></span></ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.BOOL">{{yesNoLabel(value) | translate}}</ng-container>
        <div *ngSwitchCase="ValueCaseInTpl.STRING_LONG" style="text-align: justify;" [innerHTML]="value | safeHtml"></div>
        <ng-container *ngSwitchDefault><span [innerHTML]="value | safeHtml"></span></ng-container>
      </ng-container>
    </ng-template>
    <ng-template #docValueTemplate let-value="value">
      <ng-container [ngSwitch]="calculateFieldView(value)">
        <ng-container *ngSwitchCase="FieldCaseInTpl.OBJ_LABEL">
          <ng-container [ngTemplateOutlet]="simpleValueTemplate" [ngTemplateOutletContext]="{value: value.label}"></ng-container>
        </ng-container>
        <ng-container *ngSwitchCase="FieldCaseInTpl.OBJ_LINK">
          <a target="_blank" class="link-black" [href]="value.link" [innerHTML]="value.label | safeHtml"></a>
        </ng-container>
        <ng-container *ngSwitchCase="FieldCaseInTpl.OBJ_FACET">
          <button class="btn btn-black"
                  (click)="showSearchPageWith(value.selection)"
                  title="{{value.label}}"
                  [innerHTML]="value.label | safeHtml"
          ></button>
        </ng-container>
        <ng-container *ngSwitchDefault>
          <ng-container [ngTemplateOutlet]="simpleValueTemplate" [ngTemplateOutletContext]="{value: value}"></ng-container>
        </ng-container>
      </ng-container>
    </ng-template>
    <div class="row detailed-field" *ngIf="showField()">
      <div class="detailed-field__label col-12 col-sm-3">
        {{getLabel()}}:
      </div>
      <div class="detailed-field__value col-12 col-sm-9">
        <ng-container *ngIf="getValue(); let resolvedValue">
          <div *ngIf="meta && (!meta.repeatable || (resolvedValue && resolvedValue.length === 1))">
            <ng-container [ngTemplateOutlet]="docValueTemplate"
                          [ngTemplateOutletContext]="{value: resolvedValue ? resolvedValue[0] : undefined}"></ng-container>
          </div>
          <ul *ngIf="meta && meta.repeatable && resolvedValue && resolvedValue.length > 1">
            <li *ngFor="let value of resolvedValue">
              <ng-container [ngTemplateOutlet]="docValueTemplate" [ngTemplateOutletContext]="{value: value}"></ng-container>
            </li>
          </ul>
          <ul *ngIf="!meta && resolvedValue && resolvedValue.length > 0">
            <li *ngFor="let value of resolvedValue">
              <ng-container [ngTemplateOutlet]="docValueTemplate" [ngTemplateOutletContext]="{value: value}"></ng-container>
            </li>
          </ul>
        </ng-container>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailedFieldComponent {
  @Input() fieldId: string;
  @Input() label: string | LocalizedLabel;
  @Input() value: string | number | boolean | LocalizedValue;
  @Input() meta: DocFieldEdit;

  FieldCaseInTpl = FieldCase;
  ValueCaseInTpl = ValueCase;

  constructor(protected translate: TranslateService,
              protected dateFormatService: DateFormatService,
              protected searchStore: Store<fromSearch.State>,
              protected resetUtilService: ResetUtilService) {
  }

  calculateFieldView(value: (number | string | DocFieldValue)): FieldCase {
    if (this.valueIsString(value)) {
      return FieldCase.STRING;
    }
    if (this.valueIsNumber(value)) {
      return FieldCase.NUMBER;
    }
    if (this.valueIsObj(value)) {
      const vo = value as DocFieldValue;
      if (vo.link) {
        return FieldCase.OBJ_LINK;
      }
      if (vo.selection) {
        return FieldCase.OBJ_FACET;
      }
      if (vo.label) {
        return FieldCase.OBJ_LABEL;
      }
    }
    return FieldCase.UNKNOWN;
  }

  calculateValueView(type: FieldType): ValueCase {
    if (type === 'date') {
      return ValueCase.DATE;
    }
    if (type === 'str') {
      return ValueCase.STRING_SHORT;
    }
    if (type === 'string_long') {
      return ValueCase.STRING_LONG;
    }
    if (type === 'boolean') {
      return ValueCase.BOOL;
    }
    return ValueCase.OTHER;
  }

  formatDay(isoDate: string): string {
    return this.dateFormatService.formatLabel({value: {gte: isoDate, type: 'day'}}).label;
  }

  showField(): boolean {
    const value = this.getValue();
    return !this.meta || this.meta.show_empty || (!this.meta.show_empty && value !== undefined && value.length > 0);
  }

  getLabel(): string {
    let l: string = undefined;
    const labelType = typeof this.label;
    if (labelType === "string") {
      l = this.label as string;
    } else if (labelType === "object") {
      const currentLang = this.translate.currentLang;
      const labelObj = this.label as LocalizedLabel;
      let lv = labelObj[environment.defaultLanguage || 'de'];
      if (currentLang in labelObj) {
        lv = labelObj[currentLang];
      }
      if (lv) {
        if (typeof lv === "string") {
          l = lv as string;
        } else {
          l = lv.join(" ");
        }
      }
    }
    if (!l) {
      l = this.fieldId;
    }
    return l;
  }

  getValue(): (number | string | boolean | DocFieldValue)[] {
    let v: (number | string | boolean | DocFieldValue)[] = [];
    const valueType = typeof this.value;
    if (valueType === "string") {
      return [this.value as string];
    } else if (valueType === "boolean") {
      return [this.value as boolean];
    } else if (valueType === "number") {
      return [this.value as number];
    } else if (valueType === "object") {
      const currentLang = this.translate.currentLang;
      const valueObj = this.value as LocalizedValue;
      v = valueObj[environment.defaultLanguage || "de"];
      if (currentLang in valueObj) {
        v = valueObj[currentLang];
      }
    }
    return v;
  }

  valueIsString(v: number | string | DocFieldValue): boolean {
    return typeof v === "string";
  }

  valueIsNumber(v: number | string | DocFieldValue): boolean {
    return typeof v === "number";
  }

  valueIsObj(v: number | string | DocFieldValue): boolean {
    return typeof v === "object";
  }

  showSearchPageWith(facetsToSelect: FacetSelection) {
    this.searchStore.dispatch(new fromQueryActions.SetResultListDisplayMode(defaultViewer()));
    this.resetUtilService.switchToSearchView().then(() => {
      this.searchStore.dispatch(new fromFormActions.SelectFacets(facetsToSelect));
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
    });
  }

  yesNoLabel(boolValue): string {
    return (boolValue === true) ? "yes" : "no";
  }
}
