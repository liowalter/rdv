/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, ElementRef, forwardRef, Input, ViewChild} from '@angular/core';
import {AutoCompleteValue} from "@app/shared/models/doc-details.model";
import {ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR} from "@angular/forms";
import {environment} from "@env/environment";
import {SettingsModel} from "@app/shared/models/settings.model";

/**
 * Displays selected values below an autocomplete field.
 */
@Component({
  selector: 'app-validatable-list',
  template: `
    <app-autocomplete *ngIf="env.detailSuggestionProxyUrl"
                      class="flex-grow-1"
                      objectId="{{objectId}}"
                      fieldId="{{fieldId}}"
                      [allowCustomValues]="!strict"
                      [clearAfterSelection]="true"
                      [required]="false"
                      [alreadyAdded]="values"
                      (changed)="addItem($event)"></app-autocomplete>
    <div *ngIf="!env.detailSuggestionProxyUrl">
      <input #stringTextField type="text">
      <button *ngIf="!env.detailSuggestionProxyUrl"
              type="button"
              class="btn btn-red btn-small"
              (click)="addItem(stringTextField.value)"
      >
        {{'detailed.edit_add_item' | translate}}
      </button>
    </div>
    <ul>
      <li *ngFor="let value of values">
        {{value.label}}
        <button
          title="{{'edit-field.remove' | translate}}"
          class="btn btn-red btn-small"
          type="button"
          (click)="delItem(value)"
        >
          X
        </button>
      </li>
    </ul>
    <div [hidden]="!invalid" class="alert alert-danger">
      {{'edit-field.value_required' | translate}}
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ValidatableListComponent),
      multi: true
    }
  ],
})
export class ValidatableListComponent implements ControlValueAccessor {
  @Input() values: AutoCompleteValue[];
  @Input() fieldId: string;
  @Input() objectId: string;
  @Input() strict: boolean;
  @Input() invalid: boolean;

  @ViewChild("stringTextField", {static: false}) inputRef: ElementRef<HTMLInputElement>;

  constructor() {
  }

  delItem(value) {
    const pos = this.values.indexOf(value);
    this.values.splice(pos, 1);
    this.onChangeFn(this.values);
  }

  addItem(item: string | AutoCompleteValue) {
    if (item) {
      if (typeof(item) === "string") {
        this.values.push(this.addTag(item.trim()));
        this.inputRef.nativeElement.value = "";
        this.onChangeFn(this.values);
      } else {
        const found = this.values.filter((v: AutoCompleteValue) =>
          item.value.id === v.value.id
        );
        if (found.length === 0) {
          this.values.push(item);
          this.onChangeFn(this.values);
        }
      }
    }
  }

  addTag(term: string): AutoCompleteValue {
    return {label: term, id: term, value: {id: term}, service_label: term};
  }

  get env(): SettingsModel {
    return environment;
  }

  // ---- ControlValueAccessor --------------------------------------------------

  public onChangeFn = (_: any) => {
  };

  public onTouchedFn = () => {
  };

  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    // NOP - is readonly
  }

  writeValue(obj: any): void {
    // NOP
  }
}
