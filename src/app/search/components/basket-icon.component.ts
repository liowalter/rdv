/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {select, Store} from "@ngrx/store";

import * as fromBasketActions from '../actions/basket.actions';
import * as fromSearch from '../reducers';
import {Observable} from "rxjs";

/**
 * Provides star symbol for adding or removing entry to current basket
 */
@Component({
  selector: 'app-basket-icon',
  template: `
    <div class="" (click)="addOrRemoveRecordInBasket()">
      <div class="">
        <span class="fa"
              [ngClass]="(currentBasket$ | async)?.ids.includes(basketElement) ?
                          'fa-star fa-star-selected' :
                          'fa-star fa-star-unselected'"></span>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasketIconComponent {
  /**
   * Id of document entry
   */
  @Input() basketElement: string;

  /**
   * Observable providing selected basket
   */
  currentBasket$: Observable<any>;

  /**
   * @ignore
   */
  private _currentBasket: any;

  /**
   * @ignore
   */
  constructor(private _searchStore: Store<fromSearch.State>) {
    this.currentBasket$ = _searchStore.pipe(select(fromSearch.getCurrentBasket));
    this.currentBasket$.subscribe(basket => {
      this._currentBasket = basket;
    });
  }

  /**
   * Checks if entry is already in basket and adds or removes entry accordingly
   */
  addOrRemoveRecordInBasket() {
    if (this._currentBasket) {
      this._searchStore.dispatch(new fromBasketActions.UpdateBasket(
        {
          basket: {
            id: this._currentBasket.id,
            changes: {
              ...this._currentBasket,
              ids:
                this._currentBasket.ids.includes(this.basketElement) ?
                  this._currentBasket.ids.filter(rec => rec !== this.basketElement) :
                  this._currentBasket.ids.concat(this.basketElement)
            }
          }
        }
      ));
    }
  }
}
