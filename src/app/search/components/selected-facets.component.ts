/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {combineLatest, Observable} from "rxjs";
import * as fromFormActions from "../actions/form.actions";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "@env/environment";
import {FacetFieldType, HistogramFieldType} from "@app/shared/models/settings.model";
import {map} from "rxjs/operators";
import {DateFormatService} from "@app/shared/services/date-format.service";

interface Group {
  pos?: string;
  members: any[];
}

const openSelectedFacets = {};

/**
 * Displays collapsible facets.
 */
@Component({
  selector: 'app-selected-facets',
  template: `
    <div class="selected-facets">
      <div class="selected-facets__container-elements">
        <div class="facet">
          <ul class="facet__list facet__list--selected">
            <ng-container *ngFor="let key of facetFieldsConfig | objectKeys">
              <ng-container *ngIf="(selectedFacetValueByKey$ | async)(key)?.length > 0">
                <li *ngFor="let g of group((selectedFacetValueByKey$ | async)(key))">
                  <button *ngIf="g.members.length === 1"
                          type="button"
                          (click)="resetFacetFilter(key, g.members[0].id, true)"
                          class="facet__entry facet__entry--selected link-black">
                    <span>{{facetFieldsConfig[key].label | translate}}: {{formatLastLabel(g, facetFieldsConfig[key].data_type)}}</span>
                  </button>
                  <app-collapsible
                    *ngIf="g.members.length > 1"
                    [key]="key + '/selected'"
                    [open]="openState(key)"
                    [autoClose]="true"
                    [title]="(facetFieldsConfig[key].label | translate) + ': ' + formatLastLabel(g, facetFieldsConfig[key].data_type)"
                    class="facet__entry--selected-group-container"
                    buttonClass="facet__entry facet__entry--selected-group link-black"
                    containerClass="facet__entry facet__entry--selected-group-toggable"
                    [showDelButton]="true"
                    (delete)="removeAllFacetValues(key)"
                    (changed)="switchOpen(key,$event)"
                  >
                    <button *ngFor="let vg of g.members"
                            type="button"
                            (mousedown)="resetFacetFilter(key, vg.id, false, $event)"
                            class="btn link-black hierarchic-facet__path-element">
                      <span>{{formatLabel(vg, facetFieldsConfig[key].data_type).label}}</span>
                    </button>
                  </app-collapsible>
                </li>
              </ng-container>
            </ng-container>
          </ul>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectedFacetsComponent {
  facetFieldsConfig: any;

  selectedFacetValueByKey$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>, protected dateFormatService: DateFormatService) {
    this.facetFieldsConfig = environment.facetFields;

    this.selectedFacetValueByKey$ = combineLatest([
      searchStore.select(fromSearch.getFacetValues),
      searchStore.select(fromSearch.getHistogramValues),
      searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((key: string) => {
      return hierarchies[key] ?
        hierarchies[key].values : (histograms[key] ?
          histograms[key].values : facets[key].values);
    })));
  }

  public resetFacetFilter(key, id, forceRemove: boolean, event?) {
    if (event) {
      event.preventDefault();
    }
    if (this.isBasicFacet(key)) {
      this.searchStore.dispatch(new fromFormActions.RemoveFacetValue({facet: key, id: id}));
      if (this.facetFieldsConfig[key].facetType === FacetFieldType.SIMPLE) {
        this.searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
      }
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HISTOGRAM) {
      if (!forceRemove && this.facetFieldsConfig[key].showAggs) {
        this.searchStore.dispatch(new fromFormActions.ResetHistogramBoundaryTo({key: key, id: id}));
      } else {
        this.searchStore.dispatch(new fromFormActions.RemoveHistogramBoundary({key: key, id: id}));
      }
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HIERARCHIC) {
      if (forceRemove) {
        this.searchStore.dispatch(new fromFormActions.RemoveHierarchicFacetValue({key: key, id: id}));
      } else {
        this.searchStore.dispatch(new fromFormActions.ResetHierarchicFacetValueTo({key: key, id: id}));
      }
    }
    this.doSearch();
  }

  protected doSearch() {
    this.searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  protected isBasicFacet(key) {
    return this.facetFieldsConfig[key].facetType === FacetFieldType.SIMPLE
      || this.facetFieldsConfig[key].facetType === FacetFieldType.SUBCATEGORY;
  }

  public removeAllFacetValues(key) {
    if (this.isBasicFacet(key)) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllFacetValue(key));
      if (this.facetFieldsConfig[key].facetType === FacetFieldType.SIMPLE) {
        this.searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
      }
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HISTOGRAM) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllHistogramBoundary(key, false));
    }
    if (this.facetFieldsConfig[key].facetType === FacetFieldType.HIERARCHIC) {
      this.searchStore.dispatch(new fromFormActions.RemoveAllHierarchicFacetValue(key));
    }
    this.doSearch();
  }

  group(facetValues: any[]) {
    facetValues.sort((f1, f2) => (f1.pos || "").length - (f2.pos || "").length);

    const groups: Group[] = [];
    for (const f of facetValues) {
      let found = false;
      if (f.value.pos) {
        for (const g of groups) {
          if (g.pos.length > 0 && f.value.pos.indexOf(g.pos) === 0) {
            g.members.push(f);
            found = true;
          }
        }
      }
      if (!found) {
        groups.push({pos: f.value.pos || "", members: [f]});
      }
    }

    return groups;
  }

  formatLabel(v, type: HistogramFieldType) {
    if (type === HistogramFieldType.DATE && !v.value.type) {
      v = {...v, value: {...v.value, type: 'day-range'}};
    }
    return this.dateFormatService.formatLabel(v);
  }

  formatLastLabel(g: Group, type: HistogramFieldType) {
    return this.formatLabel(g.members[g.members.length - 1], type).label;
  }

  openState(key: string): boolean {
    return !!openSelectedFacets[key];
  }

  switchOpen(key: string, open: boolean) {
    openSelectedFacets[key] = open;
  }
}
