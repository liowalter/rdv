/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';

import * as fromSearch from "../reducers";
import * as fromFormActions from "../actions/form.actions"
import {environment} from '../../../environments/environment';
import {FacetFieldType} from "../../shared/models/settings.model";

/**
 * Displays counts and combine operator for one specific facet
 */
@Component({
  selector: 'app-facets',
  template: `
    <ng-container *ngFor="let key of simpleFilter(facetFieldsConfig | objectKeys)">
      <!-- DIV pro Facette fuer Facettenwerte -->
      <div *ngIf="(shownFacetOrRange$ | async) === 'facet-pills-' + key"
           [class.active]="facetFieldsConfig[key].order == 1"
           class="tab-pane list-group"
           id="facet-pills-{{key}}"
           role="tabpanel"
           aria-labelledby="'facet-pills-'+key+'-tab'">

        <!-- Select fuer Auswahl wie die Facetten-Werte kombiniert werden sollen (OR vs. AND) -->
        Werte werden per
        <!-- bei mehreren Operatoren ein Select anzeigen, sonst nur den Wert des Operators -->
        <div *ngIf="facetFieldsConfig[key].operators.length > 1;then operatorSelect else singleOperator"></div>

        <!-- Select wie Facettenwerte dieser Facette verknuepft werden sollen -->
        <ng-template #operatorSelect>
          <select title="Verknüpfungsart" class="btn btn-sm" #sel (change)="changeOperator(key, sel.value)">
            <option *ngFor="let operator of facetFieldsConfig[key].operators"
                    [value]="operator" [selected]="operator === (facetFieldByKey$ | async)(key).operator">{{operator}}
            </option>
          </select>
        </ng-template>

        <!-- Anzeige wie Facettenwerte dieser Facetten immer verknupeft werden -->
        <ng-template #singleOperator>{{facetFieldsConfig[key].operator}}</ng-template>

        verküpft

        <!-- Liste der Facettenwerte dieser Facette -->
        <ng-container *ngIf="(facetFieldByKey$ | async)(key)">
          <ng-container *ngFor="let value of (facetFieldCountByKey$ | async)((facetFieldByKey$ | async)(key).field)">

            <!-- Facettenwert (z.B. Dissertation) und Anzahl (43) anzeigen und Button zum Auswaehlen anbieten -->
            <button *ngIf="!isSelected((facetFieldByKey$ | async)(key), value.id)"
                    (click)="selectFacet(key, value)"
                    type="button"
                    class="list-group-item list-group-item-action p-1">
                <span class="justify-content-between align-items-center d-flex">
                  <span style="word-break: break-word">{{value.label}}</span>
                  <span class="badge badge-pill badge-secondary">{{value.count}}</span>
                </span>
            </button>
          </ng-container>
        </ng-container>
      </div>
    </ng-container>
  `,
  styles: [`
    select {
      width: auto;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FacetsComponent {
  facetFieldsConfig: any;
  facetFieldByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;
  shownFacetOrRange$: Observable<string>;

  constructor(protected searchStore: Store<fromSearch.State>) {
    this.facetFieldsConfig = environment.facetFields;
    this.shownFacetOrRange$ = searchStore.pipe(select(fromSearch.getShownFacetOrRange));
    this.facetFieldByKey$ = searchStore.pipe(select(fromSearch.getFacetValuesByKey));
    this.facetFieldCountByKey$ = searchStore.pipe(select(fromSearch.getFacetFieldCountByKey));
  }

  selectFacet(field, value) {
    this.searchStore.dispatch(new fromFormActions.AddFacetValue({facet: field, id: value.id, label: value.label, value: value.value}));
  }

  changeOperator(facet: string, value: string) {
    this.searchStore.dispatch(new fromFormActions.UpdateFacetOperator({facet: facet, value: value}));
  }

  simpleFilter(facetConfigKeys) {
    return facetConfigKeys.filter((k) => this.facetFieldsConfig[k].facetType === FacetFieldType.SIMPLE
      || this.facetFieldsConfig[k].facetType === FacetFieldType.SUBCATEGORY);
  }

  public isSelected(facet, id): boolean {
    const selected = facet.values.filter((f) => f.id === id);
    return selected.length !== 0;
  }
}
