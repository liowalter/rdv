/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromSearch from '../reducers';
import * as fromQueryActions from "../actions/query.actions";
import * as fromFormActions from "../actions/form.actions";
import {take} from "rxjs/operators";
import {Subject} from "rxjs/internal/Subject";
import {AutoCompleteValue} from "@app/shared/models/doc-details.model";
import {Subscription} from "rxjs/internal/Subscription";
import {environment} from '@env/environment';
import {SimpleSearchAutocompleteComponent} from "@app/search/components/simple-search-autocomplete.component";

/**
 * Displays text search fields
 */
@Component({
  selector: 'app-simple-search',
  template: `
    <div class="simple-search">
      <h3 class="simple-search__title">{{'simple-search.title' | translate}}</h3>
      <!-- TODO what if several searchFields are configured? -->
      <div class="row no-gutters simple-search__field-container" *ngFor="let field of searchFields$ | async | objectKeys">
        <div class="col-8 position-relative">
          <input
            *ngIf="!showAutocomplete"
            class="form-control form-control-sm simple-search__field"
            type="search"
            [value]="(searchFieldByKey$ | async)(field).value"
            #searchValue
            (keyup)="updateClearButton()"
            (keydown.enter)="updateSearchValue(field)"
            placeholder="{{'simple-search.enter_search_phrase' | translate}}">
          <button
            *ngIf="!showAutocomplete"
            class="simple-search__clear"
            [class.simple-search__clear--hidden]="(searchFieldByKey$ | async)(field).value.length === 0"
            title="{{'simple-search.clear_title' | translate}}"
            #clearSearch
            (click)="clearSearchHandler()"></button>

          <app-simple-search-autocomplete
            *ngIf="showAutocomplete"
            #ngSelect
            field="{{field}}"
            [initialValues]="getInitialValues()"
            (add)="selectedSearchWord($event, field)"
            (remove)="unselectSearchWord($event, field)"
            [typeahead]="searchTerm"
            [keyDownFn]="keydown"
          ></app-simple-search-autocomplete>

          <div class="form-control simple-search__filter">
            <input id="keepFilters" type="checkbox" [checked]="keepSearchFilters$ | async" (change)="changeKeepFiltersCheckbox($event)"/>
            <label for="keepFilters">{{'simple-search.keep_filters' | translate}}</label>
          </div>
        </div>
        <div class="col-4">
          <button type="button" class="btn btn-red simple-search__search simple-search__search--large d-none d-sm-block"
                  (click)="updateSearchValue(field)">
            {{'simple-search.search' | translate}}
          </button>
          <button type="button" class="btn btn-red simple-search__search simple-search__search--small d-block d-sm-none"
                  (click)="updateSearchValue(field)">
          </button>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchComponent implements AfterViewInit, OnDestroy {
  searchFields$: Observable<any>;
  searchFieldByKey$: Observable<any>;
  keepSearchFilters$: Observable<boolean>;
  @ViewChild("clearSearch") clearSearchButtonRef: ElementRef<HTMLButtonElement>;
  @ViewChild("searchValue") inputRef: ElementRef<HTMLInputElement>;
  @ViewChild("ngSelect") ngSelect: SimpleSearchAutocompleteComponent;

  hiddenClearStyle = "simple-search__clear--hidden";

  searchTerm: Subject<string>;
  loading: Subject<boolean>;
  searchFieldsSubscription: Subscription;

  showAutocomplete = false;


  constructor(protected searchStore: Store<fromSearch.State>) {
    this.searchFields$ = searchStore.pipe(select(fromSearch.getSearchValues));
    this.searchFieldByKey$ = searchStore.pipe(select(fromSearch.getSearchValuesByKey));
    this.keepSearchFilters$ = searchStore.pipe(select(fromSearch.keepSearchFilters));

    this.keydown = this.keydown.bind(this);

    if (environment.suggestSearchWordProxyUrl) {
      const facetsForAutocomplete = Object.keys(environment.facetFields)
        .filter(facetKey => environment.facetFields[facetKey].facetType === 'simple'
          && environment.facetFields[facetKey].autocomplete_size > 0);
      this.showAutocomplete = facetsForAutocomplete.length > 0;
    }
  }

  protected updateAutoComplete(values) {
    this.ngSelect.setSelectedValues(this.parseSearchWords(values));
  }

  protected parseSearchWords(values) {
    const newAutocompleteValues = [];
    const words: string[] = values[Object.keys(values)[0]].value;
    if (words.length > 0) {
      words.forEach(w => {
        newAutocompleteValues.push(this.ngSelect.addTag(w));
      });
    }
    return newAutocompleteValues;
  }

  selectedSearchWord(x, field) {
    this.updateSearchValue(field);
  }

  unselectSearchWord(x, field) {
    this.updateSearchValue(field);
  }

  keydown(event, field: string): boolean {
    if (event.key === 'Enter') {
      this.ngSelect.pushSelectedValues(this.ngSelect.addTag(event.target.value));
      this.updateSearchValue(field);
      return false;
    }
    return true;
  }

  updateClearButton() {
    const value: string = this.inputRef.nativeElement.value;
    const clearClassList = this.getClearClasses();
    if (value.length > 0) {
      clearClassList.remove(this.hiddenClearStyle);
    } else {
      clearClassList.add(this.hiddenClearStyle);
    }
  }

  protected getClearClasses(): DOMTokenList {
    return this.clearSearchButtonRef.nativeElement.classList;
  }

  clearSearchHandler() {
    this.inputRef.nativeElement.value = "";
    this.getClearClasses().add(this.hiddenClearStyle);
  }

  setKeepFiltersCheckbox(flag: boolean) {
    this.searchStore.dispatch(new fromFormActions.KeepSearchFilters(flag));
  }

  changeKeepFiltersCheckbox(event) {
    this.setKeepFiltersCheckbox(event.currentTarget.checked);
  }

  protected getKeepFiltersCheckbox(): boolean {
    let keepFilters;
    this.keepSearchFilters$.pipe(take(1)).subscribe((flag) => keepFilters = flag);
    return keepFilters;
  }

  updateSearchValue(fieldName: string) {
    // subscribe once only ...
    this.searchFields$.pipe(take(1)).subscribe((oldValue) => {
      const keepFiltersCheckbox = this.getKeepFiltersCheckbox();
      this.setKeepFiltersCheckbox(false);
      let value: string[];
      if (this.showAutocomplete) {
        value = this.ngSelect.getSelectedValues().map((item) => item.value.id);
      } else {
        value = [];
        const iv = this.inputRef.nativeElement.value.trim();
        if (iv.length > 0) {
          value.push(iv);
        }
      }
      const reset = !keepFiltersCheckbox && (value !== oldValue);
      if (reset) {
        this.searchStore.dispatch(new fromQueryActions.Reset());
      }
      this.searchStore.dispatch(new fromFormActions.UpdateSearchFieldValue({field: fieldName, value: value}));
      this.searchStore.dispatch(new fromQueryActions.SetOffset(0));
      this.searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
      this.searchStore.dispatch(new fromQueryActions.SimpleSearch());
    });
  }

  getInitialValues(): AutoCompleteValue[] {
    let searchWords: AutoCompleteValue[] = [];
    if (this.ngSelect) {
      this.searchFields$.pipe(take(1)).subscribe((oldValue) => {
        searchWords = this.parseSearchWords(oldValue);
      });
    }
    return searchWords;
  }

  ngOnDestroy(): void {
    if (this.searchFieldsSubscription) {
      this.searchFieldsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    // depends on SimpleSearchAutocompleteComponent
    if (this.ngSelect) {
      this.searchFieldsSubscription = this.searchFields$.subscribe((values) => this.updateAutoComplete(values));
    }
  }
}
