/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input, OnDestroy} from '@angular/core';
import * as fromQueryActions from "../actions/query.actions";
import {Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {Subscription} from "rxjs/internal/Subscription";
import {Observable} from "rxjs/internal/Observable";
import {searchParamObserver} from "@app/shared/models/observed-util";
import {I18nToastrService} from "@app/shared/services/i18n-toastr.service";

/**
 * Embed IIIF universal viewer manifest link suitable for Mirador.
 */
@Component({
  selector: 'app-uv-manifest-link',
  template: `
    <div class="uv-manifest-link-container" [class.uv-manifest-link-container--open]="persistUrl">
      <div class="uv-manifest-link d-flex align-items-center">
        <div>
          <button
            class="ml-0 uv-manifest-link__action uv-manifest-link__show-question"
            [disabled]="persistUrl || showUrl"
            [title]="'uv-manifest-link.show-url.question' | translate"
            (click)="persistUrl = true;"
          >
            <img src="../assets/img/logo-iiif-34x30.png" [alt]="'uv-manifest-link.iif-logo' | translate" height="30">
          </button>
        </div>
        <a *ngIf="showUrl && !persistUrl" [href]="buildMiradorUrl()" class="ml-2 uv-manifest-link__url" target="manifest">
          {{buildMiradorUrl()}}
        </a>
        <div *ngIf="persistUrl" class="col uv-manifest-link__question">
          {{'uv-manifest-link.show-url.question' | translate}}
          <button class="btn btn-red ml-3"
                  (click)="sendPersistRequest()">{{'uv-manifest-link.show-url.confirm' | translate}}
          </button>
        </div>
        <div *ngIf="persistUrl" class="p-0 text-right uv-manifest-link__buttons">
          <button class="uv-manifest-link__action uv-manifest-link__action--cancel"
                  (click)="doNotStoreUrl()"
                  title="{{'uv-manifest-link.show-url.cancel' | translate}}">
          </button>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UvManifestLinkComponent implements OnDestroy {
  @Input() manifestUrl: string;
  showUrl?: boolean;
  persistUrl?: boolean;

  protected watchSubscription: Subscription;
  protected watch$: Observable<any>;

  constructor(protected searchStore: Store<fromSearch.State>,
              protected toastr: I18nToastrService) {
    this.watch$ = searchParamObserver(searchStore);
    this.watchSubscription = this.watch$.subscribe((vals) => this.queryParamsChanged(vals));
  }

  buildMiradorUrl(): string {
    // Mirador < 3
    // return '/default_target?manifest=' + encodeURIComponent(this.manifestUrl);
    // Mirador >= 3: no Drag & Drop
    return this.manifestUrl;
  }

  sendPersistRequest() {
    this.searchStore.dispatch(new fromQueryActions.SimpleSearch({persist: true}));
    this.persistUrl = false;
    this.showUrl = true;
  }

  doNotStoreUrl() {
    this.persistUrl = false;
  }

  ngOnDestroy(): void {
    if (this.watchSubscription) {
      this.watchSubscription.unsubscribe();
    }
  }

  protected queryParamsChanged(vals: any) {
    // search query changed, hide manifest URL
    if (this.showUrl) {
      this.toastr.info("uv-manifest-link.show-url.hide-message", "uv-manifest-link.show-url.hide-title", {timeOut: 10 * 1000});
    }
    this.showUrl = false;
  }
}
