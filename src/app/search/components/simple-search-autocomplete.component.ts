import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';
import {Subject} from "rxjs";
import {AutoCompleteValue} from "@app/shared/models/doc-details.model";
import {NgSelectComponent} from "@ng-select/ng-select";
import {Observable} from "rxjs";
import * as fromSearch from "@app/search/reducers";
import {getAllSuggestions} from "@app/search/reducers";
import {debounceTime, filter, tap} from "rxjs/operators";
import {select, Store} from "@ngrx/store";
import * as fromQueryActions from "@app/search/actions/query.actions";
import {mergeMap} from "rxjs/operators";
import {BehaviorSubject, Subscription} from "rxjs";
import {Actions, ofType} from "@ngrx/effects";
import * as fromSuggestActions from "@app/search/actions/suggest-search-word.actions";

/**
 * Displays text search fields
 */
@Component({
  selector: 'app-simple-search-autocomplete',
  template: `
    <ng-select
      #ngSelect
      [items]="suggestions$ | async"
      [addTag]="addTag"
      (add)="addWord($event)"
      (remove)="delWord($event)"
      (clear)="cleared()"
      (close)="closed()"
      (blur)="onBlur($event)"
      (focus)="onFocus()"
      [clearSearchOnAdd]="false"
      [clearable]="true"
      groupBy="group"
      bindLabel="service_label"
      [multiple]="true"
      [typeahead]="searchTerm"
      [keyDownFn]="simpleSearchKeyDownFn"
      [compareWith]="compareItems"
      [(ngModel)]="selectedValues"
      [loading]="loading$ | async"
      loadingText="{{'autocomplete.loading' | translate}}"
      addTagText="{{'autocomplete.add_item' | translate}}"
      clearAllText="{{'autocomplete.clear_all' | translate}}"
      notFoundText="{{'autocomplete.nothing_found' | translate}}"
      typeToSearchText="{{'autocomplete.type_to_search' | translate}}"
      placeholder="{{'autocomplete.type_to_search' | translate}}"
    >
      <ng-template ng-optgroup-tmp let-item="item">
        {{item.group || ('autocomplete.unnamed_group' | translate) }}
      </ng-template>
    </ng-select>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleSearchAutocompleteComponent implements OnDestroy {
  @Input() field: string;
  @Input() typeahead: Subject<string>;
  @Input() initialValues: AutoCompleteValue[];

  selectedValues: AutoCompleteValue[] = [];
  searchTerm: Subject<string>;
  suggestions$: Observable<AutoCompleteValue[]>;
  loadingSubject: Subject<boolean>;
  loading$: Observable<boolean>;
  suggestDoneSubscription: Subscription;
  currentValue = "";
  @ViewChild("ngSelect", {static: false}) ngSelect: NgSelectComponent;

  @Output() add = new EventEmitter();
  @Output() remove = new EventEmitter();

  @Input() keyDownFn = (event: KeyboardEvent, field: string) => true;


  constructor(protected searchStore: Store<fromSearch.State>, protected actions$: Actions) {
    this.loadingSubject = new BehaviorSubject(false);
    this.loading$ = this.loadingSubject.asObservable();
    this.suggestDoneSubscription = actions$
      .pipe(ofType(fromQueryActions.QueryActionTypes.SuggestSearchWordSuccess))
      .subscribe(() => {
        // if response is cached, answer is returned immediately, so that this
        // line comes too early!
        window.setTimeout(() => this.loadingSubject.next(false), 250);
      });

    this.searchTerm = new Subject<string>();
    this.suggestions$ = this.searchTerm.pipe(
      tap((v) => {
        if (v !== null) {
          const oldCurrentValue = this.currentValue;
          this.currentValue = v;
          // if user removes chars below 3, clear suggestions
          if (v.length < 3 && oldCurrentValue.length >= 3) {
            this.clearSuggestions();
          }
        }
      }),
      filter(x => x !== null && x !== "" && x.length >= 3),
      // distinctUntilChanged(), // because suggestions are cleared below 3 chars
      // no suggestions would be displayed, if user repeats search key
      debounceTime(350),
      // pipe will pass through last valid search term with 3 chars if BACKSPACE
      // is repeated and less than 3 chars remain
      filter((v) => this.currentValue.length >= 3),
      mergeMap(v => {
        searchStore.dispatch(new fromQueryActions.SuggestSearchWordSearch({text: v, selected: this.selectedValues}));
        this.loadingSubject.next(true);
        return searchStore.pipe(select(getAllSuggestions));
      })
    );
    this.simpleSearchKeyDownFn = this.simpleSearchKeyDownFn.bind(this);
  }

  simpleSearchKeyDownFn(event): boolean {
    let goOn = this.keyDownFn(event, this.field);
    if (event.key === "Escape") {
      this.currentValue = "";
      goOn = false;
    }
    if (!goOn) {
      event.target.value = "";
      this.ngSelect.close();
    }
    return goOn;
  }

  clearSuggestions() {
    this.searchStore.dispatch(new fromSuggestActions.SetSearchWordSuggestionsResults({
      results: []
    }));
  }

  compareItems(a, b): boolean {
    return a.id === b.id;
  }

  addWord(event) {
    this.currentValue = "";
    this.add.emit(event);
  }

  delWord(event) {
    this.currentValue = "";
    this.remove.emit(event);
  }

  cleared() {
    this.currentValue = "";
    this.clearSuggestions();
    this.remove.emit("");
  }

  closed() {
    this.clearSuggestions();
  }

  public getSelectedValues(): AutoCompleteValue[] {
    return this.selectedValues;
  }

  public setSelectedValues(newValues: AutoCompleteValue[]) {
    this.selectedValues = newValues;
  }

  public pushSelectedValues(newValue: AutoCompleteValue) {
    this.selectedValues.push(newValue);
  }

  ngOnDestroy(): void {
    if (this.suggestDoneSubscription) {
      this.suggestDoneSubscription.unsubscribe();
    }
  }

  public addTag(term: string): AutoCompleteValue {
    return {label: term, id: term, value: {id: term}, service_label: term};
  }

  onFocus() {
    this.currentValue = "";
  }

  onBlur(event) {
    const newSearchTerm = this.currentValue;
    this.currentValue = "";
    if (newSearchTerm) {
      // safari doesn't know event.relatedTarget
      // let browser focus new element ... if it is the search button do a search
      setTimeout(() => {
        if (document.activeElement && document.activeElement.tagName === "BUTTON") {
          const cssClasses = document.activeElement.classList;
          if (cssClasses && cssClasses.contains("simple-search__search")) {
            this.selectedValues.push(this.addTag(newSearchTerm));
            this.addWord(newSearchTerm);
          }
        }
      }, 250);
    }
  }
}
