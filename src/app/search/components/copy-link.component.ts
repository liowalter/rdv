/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {environment} from '../../../environments/environment';

/**
 * @ignore
 */
declare var LZString: any;

/**
 * Provides a button to generate an URL containing JSON data in encoded form
 */
@Component({
  selector: 'app-copy-link',
  template: `
      <button [ngClass]="{'p-1': small}" class="btn btn-primary fa fa-link"
              ngxClipboard
              [cbContent]="generateQueryLink(data, mode)">
      </button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyLinkComponent {
  /**
   * The data to be encoded. JSON is expected
   */
  @Input() data;
  /**
   * Key name of encoded data (i.e. the value)
   */
  @Input() mode = 'search';
  /**
   * Display small button
   */
  @Input() small = false;

  /**
   * Generates URL containing encoded data
   * @param {Object} jsonObject JSON data
   * @param {string} mode Key name of encoded data
   */
  // noinspection JSMethodCanBeStatic
  generateQueryLink(jsonObject: any, mode: string): string {
    const jsonString = JSON.stringify(jsonObject);
    const lzString = LZString.compressToEncodedURIComponent(jsonString);
    return environment.baseUrl + "/search?" + mode + "=" + lzString;
  }
}
