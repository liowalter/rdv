import {ChangeDetectionStrategy, Component} from "@angular/core";
import {environment} from '@env/environment';
import {HeaderSettingsModel, headerSettingsModelByLanguage, SettingsModel} from "@app/shared/models/settings.model";
import * as fromSearch from "../reducers";
import {select, Store} from "@ngrx/store";
import {BehaviorSubject, Observable} from "rxjs";
import {LocalizeRouterService} from '@gilsdav/ngx-translate-router';
import {TranslateService} from "@ngx-translate/core";
import {ActivatedRoute, NavigationExtras} from "@angular/router";
import {ResetUtilService} from "@app/shared/services/reset-util.service";

@Component({
  selector: 'app-top',
  template: `
    <ng-container *ngIf="(headerSettings$ | async)?.disable !== true">
      <div class="top">
        <ng-container *ngIf="(headerSettings$ | async)?.disableLanguageBar !== true">
          <div class="top__first-line-wrapper">
            <div class="top__first-line d-flex justify-content-center justify-content-sm-end">
              <div class="top__first-line-content">
                <a class="btn nav-about" *ngIf="!!env.externalAboutUrl"
                   href="{{externalUrl(env.externalAboutUrl)}}"
                   target="_blank">{{'header.about' | translate}}</a>
                <a class="btn nav-about" *ngIf="!!env.externalHelpUrl"
                   href="{{externalUrl(env.externalHelpUrl)}}"
                   target="_blank">{{'header.help' | translate}}</a>
                <button class="btn btn__lang" type="button" (click)="changeLanguage('de')"
                        [disabled]="langButtonDisabled('de')">de
                </button>
                <button class="btn btn__lang" type="button" (click)="changeLanguage('en')"
                        [disabled]="langButtonDisabled('en')">en
                </button>
                <button *ngIf="(headerSettings$ | async)?.showFrenchLanguageSwitch === true"
                        class="btn btn__lang" type="button" (click)="changeLanguage('fr')"
                        [disabled]="langButtonDisabled('fr')">fr
                </button>
                <button *ngIf="(headerSettings$ | async)?.showItalianLanguageSwitch === true"
                        class="btn btn__lang" type="button" (click)="changeLanguage('it')"
                        [disabled]="langButtonDisabled('it')">it
                </button>
              </div>
            </div>
          </div>
        </ng-container>
        <div class="top__owner-wrapper">
          <div class="top__owner">
            <div class="top__owner-content">
              <div>
                <img class="top__owner-content-logo" src="../../../assets/img/uni-basel-logo.svg">
              </div>
              <div class="top__owner-content-title">
                <a class="btn" routerLink="/">{{envLogoSubtitle(headerSettings$ | async) | translate}}</a>
              </div>
            </div>
            <ng-container *ngIf="(headerSettings$ | async)?.useDepartmentLogoUrl">
              <div class="top__owner-department-logo d-md-inline-block">
                <a href="{{'top.headerSettings.departmentUrl' | translate}}" target="_blank">
                  <img src="{{'top.headerSettings.departmentLogoUrl' | translate}}"/>
                </a>
              </div>
            </ng-container>
            <div style="clear: both;"></div>
          </div>
          <ng-container *ngIf="(headerSettings$ | async)?.showPortalName === true">
            <div class="top__subtitle-wrapper">
              <div class="top__subtitle">
                <button
                  class="button--no-decoration"
                  (click)="resetUtilService.reload()"
                  title="{{'top.headerSettings.resetSearch' | translate}}"
                >
                  {{envName(headerSettings$ | async) | translate}}
                </button>
              </div>
            </div>
          </ng-container>
          <!-- Hides currently not implemented features
          <div class="top__owner-content-status">
            <div class="top__owner-content-status-stored-results ml-3" *ngIf="(numberOfBaskets$ | async)">
              <span class="label">{{'header.basket' | translate}}</span> {{(numberOfBaskets$ | async)}}
            </div>
            <div class="top__owner-content-status-stored-searches ml-3" *ngIf="(numberOfSavedQueries$ | async)">
              <span class="label">{{'header.saved_queries' | translate}}</span> {{(numberOfSavedQueries$ | async)}}
            </div>
          </div> -->
        </div>
      </div>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopComponent {
  numberOfSavedQueries$: Observable<number>;
  numberOfBaskets$: Observable<number>;
  headerSettingsSubject: BehaviorSubject<HeaderSettingsModel>;
  headerSettings$: Observable<HeaderSettingsModel>;

  constructor(
    private _searchStore: Store<fromSearch.State>,
    protected localize: LocalizeRouterService,
    protected translate: TranslateService,
    protected route: ActivatedRoute,
    protected resetUtilService: ResetUtilService
  ) {
    this.numberOfSavedQueries$ = _searchStore.pipe(select(fromSearch.getSavedQueriesCount));
    this.numberOfBaskets$ = _searchStore.pipe(select(fromSearch.getBasketCount));
    this.headerSettingsSubject = new BehaviorSubject(undefined);
    this.languageChanged(translate.currentLang);
    this.headerSettings$ = this.headerSettingsSubject.asObservable();
  }

  get env(): SettingsModel {
    return environment;
  }

  externalUrl(definition: string | { [key: string]: string }): string {
    if (typeof(definition) === "string") {
      return definition;
    }
    const defaultLang = environment.defaultLanguage;
    const currLang = this.translate.currentLang;
    let lang = defaultLang;
    if (definition[currLang]) {
      lang = currLang;
    }
    return definition[lang];
  }

  changeLanguage(newLanguage: string) {
    const extras: NavigationExtras = {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    };
    // don't use useNavigateMethod=false, because this will merge query
    // params with "," instead of repeating them
    this.localize.changeLanguage(newLanguage, extras, true);
    this.languageChanged(newLanguage);
  }

  protected languageChanged(newLanguage) {
    const headerSettings = headerSettingsModelByLanguage(newLanguage, environment.headerSettings);
    this.headerSettingsSubject.next(headerSettings);
  }

  isCurrentLang(langCode: string): boolean {
    return this.translate.currentLang === langCode;
  }

  langButtonDisabled(langCode: string): string {
    if (this.isCurrentLang(langCode)) {
      return "disabled";
    }
    return "";
  }

  envLogoSubtitle(headerSettingsModel: HeaderSettingsModel): string {
    if (!headerSettingsModel) {
      return "";
    }
    if (headerSettingsModel.useLogoSubTitle) {
      return "top.headerSettings.logoSubTitle";
    }
    return this.envName(headerSettingsModel);
  }

  envName(headerSettingsModel: HeaderSettingsModel): string {
    return (headerSettingsModel && headerSettingsModel.name || "top.headerSettings.name")
  }

}
