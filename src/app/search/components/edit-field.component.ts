/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {
  AutoCompleteValue,
  DocFieldEdit,
  EditDocField,
  FieldType,
  LocalizedLabel,
  SelectedAutoCompletValue,
  ValueCase
} from "@app/shared/models/doc-details.model";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "@env/environment";
import {HttpClient} from "@angular/common/http";
import {FormGroup} from "@angular/forms";
import {SettingsModel} from "@app/shared/models/settings.model";

/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-edit-field',
  template: `
    <ng-template #simpleValueTemplate let-value="value">
      <ng-container [ngSwitch]="calculateValueView(meta.data_type)">
        <ng-container *ngSwitchCase="ValueCaseInTpl.DATE">
          <div [formGroup]="formGroup">
            <input #dateInput type="date"
                   [formControlName]="fieldId"
                   [value]="value?.value?.id"
                   (change)="setItem(dateInput.value)">
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.STRING_SHORT">
          <div [formGroup]="formGroup">
            <input #stringTextField
                   [formControlName]="fieldId"
                   type="text"
                   (change)="setItem(stringTextField.value)">
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.INT">
          <div [formGroup]="formGroup">
            <input #numberTextField
                   type="number"
                   [formControlName]="fieldId"
                   value="{{value?.value?.id}}"
                   (input)="setItem(numberTextField.value)">
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.BOOL">
          <div [formGroup]="formGroup">
            <input #boolTextField
                   type="checkbox"
                   [formControlName]="fieldId"
                   value="on"
                   [checked]="value?.value?.id"
                   (change)="setItem(boolTextField.checked)">
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.STRING_LONG">
          <div [formGroup]="formGroup">
            <textarea #bigTextField
                      [formControlName]="fieldId"
                      (change)="setItem(bigTextField.value)">{{value?.value?.id}}</textarea>
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.STRING_HTML">
          <div [formGroup]="formGroup">
            <app-tinymce [html]="(value?.value?.id) ? value.value.id : ''" (changedHtml)="setItem($event)"></app-tinymce>
          </div>
        </ng-container>
        <ng-container *ngSwitchCase="ValueCaseInTpl.SERVICE">
          <div [formGroup]="formGroup" *ngIf="env.detailSuggestionProxyUrl">
            <app-autocomplete
              class="flex-grow-1"
              objectId="{{objectId}}"
              fieldId="{{fieldId}}"
              [formControlName]="fieldId"
              [allowCustomValues]="!meta.strict"
              [clearAfterSelection]="false"
              [defaultValue]="value"
              [alreadyAdded]="getValue()"
              (changed)="setItem($event)"></app-autocomplete>
          </div>
          <div [formGroup]="formGroup" *ngIf="!env.detailSuggestionProxyUrl">
            <input #serviceNoCompleteTextField
                   [formControlName]="fieldId"
                   type="text"
                   (change)="setItem(serviceNoCompleteTextField.value)">
          </div>
        </ng-container>
        <ng-container *ngSwitchDefault>
          {{meta.data_type}}: {{value?.value?.id}}
        </ng-container>
      </ng-container>
      <div [hidden]="!isInvalid()" class="alert alert-danger">
        {{'edit-field.value_required' | translate}}
      </div>
    </ng-template>

    <div class="row edit-detailed-field" *ngIf="showField()">
      <div class="edit-detailed-field__label col-12 col-sm-3">
        {{getLabel()}}:
      </div>
      <div class="edit-detailed-field__value col-12 col-sm-9">
        <div *ngIf="meta && !meta.repeatable">
          <ng-container [ngTemplateOutlet]="simpleValueTemplate"
                        [ngTemplateOutletContext]="{value: singleValue()}"></ng-container>
        </div>
        <div *ngIf="meta && meta.repeatable">
          <div [formGroup]="formGroup">
            <app-validatable-list
              [values]="getValue()"
              objectId="{{objectId}}"
              fieldId="{{fieldId}}"
              [strict]="meta.strict"
              [formControlName]="fieldId"
              [invalid]="isInvalid()"
            ></app-validatable-list>
          </div>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditFieldComponent {
  @Input() objectId: string;
  @Input() fieldId: string;
  @Input() label: string | LocalizedLabel;
  @Input() value: EditDocField;
  @Input() meta: DocFieldEdit;
  @Input() formGroup: FormGroup;

  ValueCaseInTpl = ValueCase;

  constructor(protected translate: TranslateService,
              protected http: HttpClient) {
  }

  isInvalid(): boolean {
    const control = this.formGroup.controls[this.fieldId];
    return control.invalid && (control.dirty || control.touched);
  }

  calculateValueView(type: FieldType): ValueCase {
    if (this.meta.service) {
      return ValueCase.SERVICE;
    }
    if (type === 'date') {
      return ValueCase.DATE;
    }
    if (type === 'string_long') {
      return ValueCase.STRING_LONG;
    }
    if (type === 'html') {
      return ValueCase.STRING_HTML;
    }
    if (type === 'str') {
      return ValueCase.STRING_SHORT;
    }
    if (type === 'boolean') {
      return ValueCase.BOOL;
    }
    if (type === 'int') {
      return ValueCase.INT;
    }
    return ValueCase.OTHER;
  }

  showField(): boolean {
    const value = this.getValue();
    return this.meta.edit || (this.meta.show_empty || (value !== undefined && value.length > 0));
  }

  getLabel(): string {
    let l: string = undefined;
    const labelType = typeof this.label;
    if (labelType === "string") {
      l = this.label as string;
    } else if (labelType === "object") {
      const currentLang = this.translate.currentLang;
      const labelObj = this.label as LocalizedLabel;
      let lv = labelObj[environment.defaultLanguage || "de"];
      if (currentLang in labelObj) {
        lv = labelObj[currentLang];
      }
      if (lv) {
        if (typeof lv === "string") {
          l = lv as string;
        } else {
          l = lv.join(" ");
        }
      }
    }
    if (!l) {
      l = this.fieldId;
    }
    return l.trim();
  }

  getValue(): SelectedAutoCompletValue[] {
    return this.value.values;
  }

  singleValue(): SelectedAutoCompletValue {
    const v = this.getValue();
    if (v) {
      return v[0];
    }
    return undefined;
  }

  setItem(value: boolean | string | AutoCompleteValue) {
    if (value !== undefined && value !== null) {
      let v: SelectedAutoCompletValue;
      if (typeof value === 'boolean') {
        v = this.addTag(value);
      } else if (typeof value === 'string') {
        v = this.addTag(value.trim());
      } else {
        v = value;
      }
      this.value.values = [v];
    } else {
      this.value.values = [];
    }
  }

  addTag(term: string | boolean): SelectedAutoCompletValue {
    const label = "" + term;
    return {label: label, id: label, value: {id: term}, service_label: label};
  }

  get env(): SettingsModel {
    return environment;
  }
}
