/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AutoCompleteValue} from "@app/shared/models/doc-details.model";
import {debounceTime, filter, map, take, tap} from "rxjs/operators";
import {deepJsonClone} from "@app/shared/utils";
import {NgSelectComponent} from "@ng-select/ng-select";
import {mergeMap} from "rxjs/operators";
import {BehaviorSubject} from "rxjs";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as fromQueryActions from "@app/search/actions/query.actions";
import {Store} from "@ngrx/store";
import * as fromSearch from "@app/search/reducers";
import {Actions, ofType} from "@ngrx/effects";
import {Observable, Subject} from 'rxjs';

/**
 * Displays an input field with auto-complete suggestions.
 */
@Component({
  selector: 'app-autocomplete',
  template: `
    <div class="d-flex">
      <div class="flex-grow-1">
        <ng-select #ngSelect
                   [items]="suggestions$ | async"
                   (change)="useValue($event)"
                   [addTag]="allowCustomValues ? addTag : false"
                   [clearSearchOnAdd]="this.clearAfterSelection"
                   [clearable]="!this.required"
                   bindLabel="service_label"
                   groupBy="group"
                   [typeahead]="searchTerm"
                   (open)="loadSuggestions('')"
                   (close)="closed()"
                   [compareWith]="compareItems"
                   [keyDownFn]="keyDownHandler"
                   [disabled]="disabled"
                   [(ngModel)]="selectedValue"
                   [loading]="loading$ | async"
                   loadingText="{{'autocomplete.loading' | translate}}"
                   addTagText="{{'autocomplete.add_item' | translate}}"
                   clearAllText="{{'autocomplete.clear_all' | translate}}"
                   notFoundText="{{'autocomplete.nothing_found' | translate}}"
                   typeToSearchText="{{'autocomplete.type_to_search' | translate}}"
                   placeholder="{{'autocomplete.type_to_search' | translate}}"
        ></ng-select>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutocompleteComponent),
      multi: true
    }
  ],
})
export class AutocompleteComponent implements OnInit, ControlValueAccessor {
  @Input() objectId: string;
  @Input() fieldId: string;
  @Input() allowCustomValues: boolean;
  @Input() required: boolean;
  @Input() clearAfterSelection: boolean;
  @Input() defaultValue?: AutoCompleteValue;
  @Input() alreadyAdded: AutoCompleteValue[];
  @Output() changed = new EventEmitter<string | AutoCompleteValue>();

  @ViewChild("ngSelect") ngSelect: NgSelectComponent;

  selectedValue: AutoCompleteValue = null;
  searchTerm: Subject<string>;
  suggestions$: Observable<Partial<AutoCompleteValue>[]>;
  loading: Subject<boolean>;
  loading$: Observable<boolean>;
  disabled = false;


  constructor(protected searchStore: Store<fromSearch.State>, protected actions$: Actions) {
    this.searchTerm = new Subject<string>();
    this.loading = new BehaviorSubject(false);
    this.loading$ = this.loading.asObservable();
    this.suggestions$ = this.searchTerm.pipe(
      filter(x => x !== null),
      debounceTime(350),
      mergeMap(v => {
        this.enableLoadingWheel(v);
        // if query is cached by ui, response comes immediately and
        // too early for the of(...) statement below
        setTimeout(() =>
            searchStore.dispatch(new fromQueryActions.MakeSuggestEditFieldSearch({
              id: this.objectId,
              field: this.fieldId,
              query: v.trim(),
              selected: this.alreadyAdded || [],
            }))
          , 250);
        return actions$.pipe(
          ofType(fromQueryActions.QueryActionTypes.MakeSuggestEditFieldSearchSuccess),
          take(1),
          map((action: fromQueryActions.MakeSuggestEditFieldSearchSuccess) => action.payload),
          tap(() => this.disableLoadingWheel())
        );
      })
    );
    this.keyDownHandler = this.keyDownHandler.bind(this);
  }

  keyDownHandler(event) {
    if (this.allowCustomValues) {
      if (event.key === 'Enter') {
        const term = event.target.value.trim();
        if (term.length > 0) {
          this.useValue(this.addTag(term));
          this.ngSelect.close();
          return false;
        }
      }
    }
    return true;
  }

  enableLoadingWheel(v) {
    this.loading.next(true);
  }

  disableLoadingWheel() {
    this.loading.next(false);
  }

  compareItems(a, b): boolean {
    return a.id === b.id;
  }

  loadSuggestions(value: string) {
    this.searchTerm.next(value);
  }

  useValue(value: AutoCompleteValue) {
    const newValue = value ? deepJsonClone(value) : undefined;
    this.onChangeFn(newValue);
    this.changed.emit(newValue);
    if (this.clearAfterSelection) {
      this.ngSelect.handleClearClick();
    }
    this.disableLoadingWheel();
    this.ngSelect.itemsList.setItems([]);
  }

  addTag(term: string): AutoCompleteValue {
    return {label: term, id: term, value: {id: term}, service_label: term};
  }

  ngOnInit(): void {
    if (this.defaultValue) {
      this.selectedValue = this.defaultValue;
    }
  }

  closed() {
    this.disableLoadingWheel();
    this.ngSelect.itemsList.setItems([]);
  }

  // ---- ControlValueAccessor --------------------------------------------------

  public onChangeFn = (_: any) => {
  };

  public onTouchedFn = () => {
  };

  registerOnChange(fn: any): void {
    this.onChangeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    // NOP
  }
}
