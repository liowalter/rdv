/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {take} from 'rxjs/operators';
import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {LocalizeRouterService} from "@gilsdav/ngx-translate-router";
import * as fromQueryActions from "../actions/query.actions";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {ActivatedRoute, Router} from "@angular/router";
import {environment} from '@env/environment';
import {SettingsModel} from "@app/shared/models/settings.model";
import {I18nToastrService} from "@app/shared/services/i18n-toastr.service";

export const LOCALIZED_ROUTER_PREFIX = 'ROUTES.';

/**
 * Landing page for external links.
 */
@Component({
  selector: 'app-popup-landing',
  template: `
    <div class="popup-landing">
      <app-top></app-top>
      <h3>{{'popup-landing.message.welcome' | translate}}</h3>
      <div *ngIf="!env.popupQueryProxyUrl" class="errortext">Server doesn't support "popup" deep linking.</div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PopupLandingComponent implements AfterViewInit, OnDestroy {
  protected popupSubscriber: Subscription;

  /**
   * @ignore
   */
  constructor(protected searchStore: Store<fromSearch.State>,
              protected localize: LocalizeRouterService,
              protected router: Router,
              protected activatedRoute: ActivatedRoute,
              protected toastr: I18nToastrService) {
    this.popupSubscriber = this.searchStore.pipe(select(fromSearch.getPopup)).subscribe((newPopup) => {
      if (newPopup) {
        const homeUrl = this.localize.translateRoute("/") as string;
        // called without language ...
        if (this.router.url.indexOf("/popup") === 0) {
          // adds manually "ROUTES.", so that localized router works again
          this.router.navigate([homeUrl + "/" + LOCALIZED_ROUTER_PREFIX], {skipLocationChange: false})
            .then(() => {
              // this.router.navigate([homeUrl], {skipLocationChange: true});
              // moved url fix to NewSearchComponent._watchSearchRequestChanges(), because
              // this.router.navigate() causes a creation of a second NewSearchComponent instance, which
              // results in cancelled ajax requests of the first instance (no functional impact, but ugly)
              this.showMessage(newPopup);
            });
        } else {
          this.router.navigateByUrl(homeUrl);
          this.showMessage(newPopup);
        }
      }
    });
  }

  protected showMessage(newPopup) {
    const key = "popup-landing.message." + newPopup.i18nKey;
    this.toastr.info({key, values: newPopup.values}, undefined, {disableTimeOut: true, closeButton: true});
  }

  ngAfterViewInit(): void {
    if (environment.popupQueryProxyUrl) {
      let popupId;
      this.activatedRoute.queryParams.pipe(take(1)).subscribe(params => popupId = params['popupid']);
      if (popupId) {
        this.searchStore.dispatch(new fromQueryActions.SimpleSearch({return_selection: true, popupId: {id: popupId}}));
      }
    }
  }

  ngOnDestroy(): void {
    if (this.popupSubscriber) {
      this.popupSubscriber.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }
}
