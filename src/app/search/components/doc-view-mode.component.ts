/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/**
 * Shows detailed information on a document
 */
import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {ListType, Page, SettingsModel} from "@app/shared/models/settings.model";
import {environment} from "@env/environment";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "@app/search/reducers";
import * as fromQueryActions from "@app/search/actions/query.actions";
import {CollapsibleComponent} from "@app/search/components/collapsible.component";

let opened = false;

@Component({
  selector: 'app-doc-view-mode',
  template: `
    <ng-template #docViewerTpl let-type="type" let-version="version" let-icon="icon">
      <button class="search-results__mode search-results__mode--{{type}}"
              [title]="('search-results.view-mode-help.' + version) | translate"
              (mousedown)="changeResultViewMode($event, version)">
        <img src="{{icon}}"/>
        {{('search-results.view-mode.' + version) | translate}}
      </button>
    </ng-template>
    <div *ngIf="env.documentViewerProxyUrl && buildViewerTplItems(resultDisplayMode$ | async).length > 0"
         class="search-results__mode-container d-sm-block d-md-inline-block align-top">
      <app-collapsible #docViewerMode
                       [open]="openState()"
                       [autoClose]="true"
                       [titleIcon]="currentViewerIcon(resultDisplayMode$ | async)"
                       titleTooltip="search-results.view-mode.title"
                       buttonClass="styled-select search-results__mode"
                       (changed)="switchOpen($event)"
      >
        <ul>
          <app-expandable
            [itemTemplate]="docViewerTpl"
            [itemsPerExpansion]="10"
            [items]="buildViewerTplItems(resultDisplayMode$ | async)"></app-expandable>
        </ul>
      </app-collapsible>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DocViewModeComponent {
  @Input() disableListView: boolean;
  @Input() allowedViewers: string[];
  @Output() changed = new EventEmitter<string>(false);
  resultDisplayMode$: Observable<string>;
  @ViewChild("docViewerMode") docViewerMode: CollapsibleComponent;

  constructor(private _searchStore: Store<fromSearch.State>) {
    this.resultDisplayMode$ = this._searchStore.pipe(select(fromSearch.getUsedResultViewer));
  }

  get env(): SettingsModel {
    return environment;
  }

  buildViewerTplItems(currentViewer: string) {
    let result = [];
    if (this.allowedViewers) {
      this.allowedViewers
        .filter(viewer => currentViewer !== viewer)
        .map(viewer => {
          result.push({
            type: environment.documentViewer[viewer].type.toString(),
            version: viewer,
            icon: environment.documentViewer[viewer].iconUrl,
          });
        });
    }
    const listMode = ListType.LIST.toString();
    if (currentViewer !== listMode && this.disableListView !== true) {
      result = [{
        type: listMode,
        version: listMode,
        icon: this.currentViewerIcon(listMode),
      }].concat(result);
    }
    return result;
  }

  currentViewerIcon(currentViewer: string) {
    if (currentViewer === ListType.LIST.toString()) {
      return "/assets/img/icon_list.svg";
    }
    return environment.documentViewer[currentViewer].iconUrl;
  }

  changeResultViewMode(event, newResultListMode: string) {
    event.preventDefault();
    this._searchStore.dispatch(new fromQueryActions.SetResultListDisplayMode(newResultListMode));
    this.docViewerMode.toggleCollapsible();
    this.changed.emit(newResultListMode);
  }

  openState(): boolean {
    return opened;
  }

  switchOpen(openFlag: boolean) {
    opened = openFlag;
  }
}
