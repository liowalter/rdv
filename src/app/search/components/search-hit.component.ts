/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

import {empty} from "@app/shared/utils";
import {SettingsModel} from "@app/shared/models/settings.model";
import {environment} from "@env/environment";
import {TranslateService} from "@ngx-translate/core";
import {Observable} from "rxjs";
import {BehaviorSubject, Subject} from "rxjs";
import {Result} from "@app/search/models/result.model";

/**
 * Displays a row in the result or  list
 */
@Component({
  selector: 'app-search-hit',
  template: `
    <ng-template #snippetTextTpl>
      <a *ngIf="!doc.fulltext && doc.fulltext_snippet"
         class="search-hit__title-link link-black text-decoration-none"
         [title]="'search-results.to_doc' | translate"
         [routerLink]="['/detail' | localize, doc.id]"
         [state]="{cursor: doc.search_after_values}">
        <div class="field" [innerHTML]="doc.fulltext_snippet | safeHtml"></div>
      </a>
      <app-collapsible *ngIf="doc.fulltext"
                       [open]="fullTextOpened$ | async"
                       titleHtml="{{doc.fulltext_snippet}}"
                       (changed)="toggleFulltext($event)"
                       buttonClass="field"
                       containerClass="field"
      >
        <span [innerHTML]="doc.fulltext | safeHtml"></span>
      </app-collapsible>
      <ng-container *ngIf="doc.link">
        <div *ngFor="let l of doc.link">
          <a class="search-hit__external-link"
             href="{{l.url}}"
             target="_blank"
          >
            <span *ngIf="!l.label">{{ 'search-results.external_link' | translate}}</span>
            <span *ngIf="l.label" [innerHTML]="l.label | safeHtml"></span>
          </a>
        </div>
      </ng-container>
    </ng-template>
    <div class="search-hit row no-gutters">
      <div class="search-hit__basket text-left text-sm-center mb-2 mb-sm-0 col-12 col-sm-1">
        <app-basket-icon *ngIf="env?.showExportList?.basket === true" [basketElement]="doc.id"></app-basket-icon>
      </div>
      <div class="search-hit__info col row no-gutters">
        <div *ngIf="env.showImagePreview !== false"
             class="col-3 search-hit__info-preview">
          <a class="search-hit__info-preview-container"
             [title]="'search-results.to_doc' | translate"
             [routerLink]="['/detail' | localize, doc.id]"
             [ngStyle]="clickable()"
             [state]="{cursor: doc.search_after_values}">
            <img [src]="buildPreviewUrls(doc.preview_image, doc.object_type)[0]"
                 [appBrokenImage]="buildPreviewUrls(doc.preview_image, doc.object_type)"
            />
          </a>
        </div>
        <ng-container *ngIf="env.showImagePreview !== false">
          <div [ngStyle]="{'flex-shrink': 0, width: '1.3rem'}"></div>
        </ng-container>
        <div class="search-hit__info-text" [class.col-8]="env.showImagePreview !== false">
          <ng-container *ngIf="empty(getObjectType())">
            <div class="search-hit__type">
              {{indexInResultList}}.
            </div>
          </ng-container>
          <ng-container *ngIf="!empty(getObjectType())">
            <div class="search-hit__type">
              {{indexInResultList}}.
              <app-search-hit-value [fieldValue]="getObjectType()"></app-search-hit-value>
            </div>
          </ng-container>
          <ng-container *ngIf="!empty(doc.title)">
            <a class="search-hit__title-link link-black"
               [title]="'search-results.to_doc' | translate"
               [ngStyle]="clickable()"
               [routerLink]="['/detail' | localize, doc.id]"
               [state]="{cursor: doc.search_after_values}">
              <h3 class="search-hit__title">
                <app-search-hit-value [fieldValue]="doc.title"></app-search-hit-value>
              </h3>
            </a>
          </ng-container>
          <div class="d-none d-sm-block d-md-block d-lg-block d-xl-block">
            <ng-container *ngIf="!empty(getLine1())">
              <h4 class="search-hit__line_1">
                <app-search-hit-value [fieldValue]="getLine1()"></app-search-hit-value>
              </h4>
            </ng-container>
            <ng-container [ngTemplateOutlet]="snippetTextTpl"></ng-container>
          </div>
        </div>
        <div class="d-block d-sm-none d-md-none d-lg-none d-xl-none">
          <ng-container *ngIf="!empty(getLine1())">
            <h4 class="search-hit__line_1">
              <app-search-hit-value [fieldValue]="getLine1()"></app-search-hit-value>
            </h4>
          </ng-container>
          <ng-container [ngTemplateOutlet]="snippetTextTpl"></ng-container>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchHitComponent {
  @Input() doc: Result;
  @Input() indexInResultList: number;

  fullTextOpenedSubject: Subject<boolean>;
  fullTextOpened$: Observable<boolean>;

  constructor(protected translate: TranslateService) {
    this.fullTextOpenedSubject = new BehaviorSubject<boolean>(false);
    this.fullTextOpened$ = this.fullTextOpenedSubject.asObservable();
  }

  toggleFulltext(open: boolean) {
    this.fullTextOpenedSubject.next(open);
  }

  objectTypeImageUrl(objectType): string {
    return '/assets/img/icon-' + objectType + '.png';
  }

  empty(obj: any): boolean {
    return empty(obj);
  }

  get env(): SettingsModel {
    return environment;
  }

  getObjectType(): string {
    const currentLang = this.translate.currentLang;
    let ot = this.doc.object_type;
    if (this.doc.i18n && this.doc.i18n[currentLang] && this.doc.i18n[currentLang].object_type) {
      ot = this.doc.i18n[currentLang].object_type;
    }
    return ot;
  }

  getLine1(): string {
    const currentLang = this.translate.currentLang;
    let l = this.doc.line1;
    if (this.doc.i18n && this.doc.i18n[currentLang] && this.doc.i18n[currentLang].line1) {
      l = this.doc.i18n[currentLang].line1;
    }
    return l;
  }

  buildPreviewUrls(previewImage, objectType): string[] {
    const list = [];
    if (previewImage) {
      if (environment.scaleDocThumbsByIiif !== false) {
        previewImage += '/full/120,/0/default.jpg';
      }
      list.push(previewImage);
    }
    list.push(this.objectTypeImageUrl(objectType));
    list.push('/assets/img/icon-default.png');
    return list;
  }

  clickable() {
    return {'pointer-events': (this.doc.fulltext || (this.doc.link && this.doc.link.length > 0)) ? "none" : "auto"};
  }
}
