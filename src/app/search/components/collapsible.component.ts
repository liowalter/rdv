/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {environment} from "@env/environment";
import {FacetFieldsModel} from "@app/shared/models/settings.model";
import {ActiveToast} from "ngx-toastr";
import {I18nToastrService} from "@app/shared/services/i18n-toastr.service";

/**
 * Displays collapsible container.
 * Don't implement OnDestroy!
 */
@Component({
  selector: 'app-collapsible',
  template: `
    <div class="collapsible">
      <div [class.collapsible--with-delete]="showDelButton || !!facetFieldsConfig[key]?.help">
        <div #tooltip class="popper" [class.popper__hidden]="!key || !facetFieldsConfig[key]?.help || isTouch()">
          <span *ngIf="key && facetFieldsConfig[key]?.help">{{facetFieldsConfig[key].help | translate}}</span>
          <span class="popper__arrow"></span>
        </div>
        <button type="button"
                [class]="buttonsClass()"
                [attr.title]="this.titleTooltip ? (this.titleTooltip | translate) : null"
                (click)="toggleCollapsible()">
          <span *ngIf="title">{{title}}</span>
          <span *ngIf="titleHtml" [innerHTML]="titleHtml | safeHtml"></span>
          <img *ngIf="titleIcon" src="{{titleIcon}}"/>
        </button>
        <button *ngIf="showDelButton"
                [title]="'collapsible.delete-all-label' | translate"
                class="button--delete"
                (click)="shallDelete()"></button>
        <div *ngIf="key && !!facetFieldsConfig[key]?.help && isTouch()" class="info-container">
          <button class="info" (click)="showInfo()">I</button>
        </div>
        <div *ngIf="key && !!facetFieldsConfig[key]?.help && !isTouch()" class="info-container">
          <button class="info" (click)="showInfo()" appPopper [target]="tooltip">I</button>
        </div>
      </div>
      <div class="collapsible__container-scroll-wrapper">
        <div *ngIf="!collapsedState()"
             [class]="containersClass()"
             [class.collapsed]="collapsedState()"
        >
          <div #dropdownContainer tabindex="5" (blur)="blur()">
            <ng-content></ng-content>
            {{focus(dropdownContainer)}}
          </div>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsibleComponent {
  @Input() open: boolean;
  @Input() buttonClass: string;
  @Input() containerClass?: string;
  @Input() title?: string;
  @Input() titleHtml?: string;
  @Input() titleIcon?: string;
  @Input() titleTooltip?: string;
  @Input() key?: string;
  @Input() showDelButton?: boolean;
  @Input() autoClose?: boolean;
  @Output() changed = new EventEmitter<boolean>(false);
  @Output() delete = new EventEmitter<boolean>(false);

  facetFieldsConfig: FacetFieldsModel;
  infoToast: ActiveToast<any>;

  constructor(protected toastr: I18nToastrService) {
    this.facetFieldsConfig = environment.facetFields;
  }

  toggleCollapsible() {
    this.changed.emit(!this.open);
  }

  shallDelete() {
    this.delete.emit(true);
  }

  buttonsClass(): string {
    let css = this.buttonClass || "";
    css += " collapsible__title";
    if (this.collapsedState()) {
      css += " collapsed";
    }
    return css;
  }

  containersClass(): string {
    let css = this.containerClass || "";
    css += " collapsible__container pl-4 pl-sm-0 pl-lg-4";
    return css;
  }

  collapsedState(): boolean {
    return !this.open;
  }

  showInfo() {
    if (this.infoToast) {
      this.toastr.clear(this.infoToast.toastId);
      this.infoToast = undefined;
    } else {
      const key = this.facetFieldsConfig[this.key].help;
      this.infoToast = this.toastr.info(key, undefined, {disableTimeOut: true, closeButton: true});
    }
  }

  isTouch() {
    return ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
  }

  blur() {
    this.changed.emit(false);
  }

  focus(dropdownContainer: HTMLElement) {
    if (this.autoClose === true) {
      dropdownContainer.focus();
    }
  }
}
