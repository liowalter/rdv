/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy} from "@angular/core";
import {BehaviorSubject, Observable, Subject, Subscription} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import {ListType, Page, SettingsModel, SortFieldsModel, ViewerType} from "@app/shared/models/settings.model";
import {environment} from "@env/environment";
import * as fromQueryActions from "../actions/query.actions";
import {debounceTime, map, skip, take} from "rxjs/operators";
import * as fromFormActions from "../actions/form.actions";
import {combineLatest} from "rxjs";
import {buildId} from "../reducers/facet.reducer";
import {BreakpointObserver} from "@angular/cdk/layout";
import {I18nToastrService} from "@app/shared/services/i18n-toastr.service";
import {availableIiifViewers, defaultViewer} from "@app/shared/models/settings-util";

@Component({
  selector: 'app-search-results',
  template: `
    <div class="search-results">
      <div class="search-results__header row pl-3 mr-1">
        <h4 class="search-results__title">{{'search-results.title' | translate:{value: (searchCount$ | async)} }}</h4>
        <div class="pr-0 pl-0 col-sm search-results__sorting align-self-center mt-2 text-left
            align-self-sm-end text-sm-right mt-xs-3 mt-sm-0">
          <div class="styled-select d-md-inline-block">
            <select
              [title]="'search-results.sort.title' | translate"
              [ngModel]="sorting"
              name="sorting"
              (ngModelChange)="switchSorting($event)">
              <option *ngFor="let sortOption of sortFields" [ngValue]="sortOption">{{sortOption.display | translate}}</option>
            </select>
          </div>
          <app-rows-per-page
            class="d-sm-block d-md-inline-block text-sm-right text-md-center"
            [rowsPerPage]="rowsPerPage$ | async"
            (changeRowsPerPage)="changeRowsPerPage($event)"></app-rows-per-page>
          <app-doc-view-mode
            [allowedViewers]="allIiifViewers()"
            (changed)="searchAfterViewModeChange()"
          ></app-doc-view-mode>
        </div>
      </div>
      <div class="mt-2 row pl-3 mr-1">
        <div class="col-12 col-sm-4 search-results__facets pl-0" *ngIf="resultAsList(resultDisplayMode$ | async)">
          <div class="d-block mb-3" [class.d-sm-none]="!(smallFacet$ | async)">
            <button class="btn btn-red collapsible-facets__toggler" (click)="toggleFacets()">
              {{'collapsible-facets.filter' | translate}}
            </button>
          </div>
          <app-selected-facets></app-selected-facets>
          <app-collapsible-facets
            (closeFacetEmitter)="toggleFacets()"
            [forceToggled]="smallFacet$ | async"
            [showFacets]="showFacets$ | async">
          </app-collapsible-facets>
        </div>
        <div class="col-12 search-results__hit-container pl-0 pr-0" [class.col-sm-8]="resultAsList(resultDisplayMode$ | async)">
          <ng-container *ngIf="resultAsList(resultDisplayMode$ | async)">
            <div class="search-results__hit"
                 *ngFor="let doc of (docs$ | async); index as i">
              <app-search-hit [doc]="doc" [indexInResultList]="i + 1"></app-search-hit>
            </div>
            <div class="row">
              <div class="col-1 d-none d-sm-block"></div>
              <div class="col-11 text-center text-sm-left" *ngIf="(docs$ | async).length < (searchCount$ | async)">
                <button type="button" class="btn search-results__more btn-red"
                        (click)="loadMore()">{{'search-results.load_more' | translate}}
                </button>
              </div>
            </div>
          </ng-container>
          <ng-container *ngIf="resultInViewer(resultDisplayMode$ | async)">
            <div class="col-12 d-flex search-results__hit-container-actions pl-0 pr-0">
              <div class="col position-relative pl-0">
                <button class="btn btn-red collapsible-facets__toggler" (click)="toggleFacets()">
                  {{'collapsible-facets.filter' | translate}}
                </button>
                <div class="search-results__facets">
                  <app-collapsible-facets
                    (closeFacetEmitter)="toggleFacets()"
                    [forceToggled]="true"
                    [showFacets]="showFacets$ | async">
                  </app-collapsible-facets>
                </div>
              </div>
              <div>
                <button *ngIf="(offset$ | async) > 0"
                        type="button"
                        class="btn search-results__more btn-red"
                        (click)="loadPrevPage()">{{'search-results.load_prev' | translate}}
                </button>
                <div class="search-results__hit-container-actions-page d-sm-inline-block">
                  Seite {{((offset$ | async) / (rowsPerPage$ | async)) + 1}}
                </div>
                <button *ngIf="((offset$ | async) + (rowsPerPage$ | async)) < (searchCount$ | async)"
                        type="button"
                        class="btn search-results__more btn-red"
                        (click)="loadNextPage()">{{'search-results.load_next' | translate}}
                </button>
              </div>
            </div>
            <app-selected-facets class="facet__list--horizontally"></app-selected-facets>
          </ng-container>
        </div>
      </div>
    </div>
    <ng-container *ngIf="resultInViewer(resultDisplayMode$ | async)">
      <div class="search-results--viewer" [class.search-results--viewer-wide]="env?.viewerExtraWide === true">
        <div class="search-results">
          <app-universal-viewer
            *ngIf="displayInUV(resultDisplayMode$ | async)"
            [version]="resultDisplayMode$ | async"
            [manifestUrl]="(iiifUrl$ | async)"></app-universal-viewer>
          <app-mirador
            *ngIf="displayInMirador(resultDisplayMode$ | async)"
            [version]="resultDisplayMode$ | async"
            [manifestUrl]="(iiifUrl$ | async)"></app-mirador>
        </div>
      </div>
      <div class="search-results">
        <app-uv-manifest-link [manifestUrl]="(iiifUrl$ | async)" class="mt-3"></app-uv-manifest-link>
      </div>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultsComponent implements AfterViewInit, OnDestroy {
  searchCount$: Observable<number>;
  iiifUrl$: Observable<string>;
  docs$: Observable<any>;
  offset$: Observable<number>;
  rowsPerPage$: Observable<number>;
  sort$: Observable<any>;
  selectedFacetValueByKey$: Observable<any>;
  resultDisplayMode$: Observable<string>;

  readonly sortFields = environment.sortFields;
  protected currentOffset = 0;
  protected rowsPerPage;

  protected offsetSubscription: Subscription;
  protected rowsPerPageSubscription: Subscription;
  protected resultDisplayModeSubscription: Subscription;

  sorting: SortFieldsModel;
  protected sortSubscriber: Subscription;

  protected breakPointSubscription: Subscription;

  showFacetsSubject: Subject<boolean>;
  showFacets$: Observable<boolean>;
  smallFacetSubject: Subject<boolean>;
  smallFacet$: Observable<boolean>;

  constructor(private _searchStore: Store<fromSearch.State>,
              private toastr: I18nToastrService,
              protected breakpointObserver: BreakpointObserver) {
    this.searchCount$ = _searchStore.pipe(select(fromSearch.getTotalResultsCount));
    this.iiifUrl$ = _searchStore.pipe(select(fromSearch.getIIIFResultUrl));
    this.docs$ = _searchStore.pipe(select(fromSearch.getAllResults));
    this.offset$ = _searchStore.pipe(select(fromSearch.getResultOffset));
    this.offsetSubscription = this.offset$.subscribe((newOffset) => {
      this.currentOffset = newOffset;
    });
    this.rowsPerPage$ = _searchStore.pipe(select(fromSearch.getResultRows));
    this.rowsPerPageSubscription = this.rowsPerPage$.subscribe((newRowsPerPage) => {
      this.rowsPerPage = newRowsPerPage;
    });

    this.sort$ = combineLatest([
      _searchStore.pipe(select(fromSearch.getResultSortField)),
      _searchStore.pipe(select(fromSearch.getResultSortOrder))
    ]);
    this.sortSubscriber = this.sort$.subscribe((v) => {
      this.sorting = this.findSortField(v[0], v[1]);
    });
    this.selectedFacetValueByKey$ = combineLatest([
      this._searchStore.select(fromSearch.getFacetValues),
      this._searchStore.select(fromSearch.getHistogramValues),
      this._searchStore.select(fromSearch.getHierarchyValues),
    ]).pipe(map(([facets, histograms, hierarchies]) => ((key: string) => {
      return hierarchies[key] ?
        hierarchies[key].values : (histograms[key] ?
          histograms[key].values : facets[key].values);
    })));

    this.resultDisplayMode$ = this._searchStore.pipe(select(fromSearch.getUsedResultViewer));

    // assume desktop, if wrong breakpointObserver fixes values immediately
    this.showFacetsSubject = new BehaviorSubject<boolean>(true);
    this.showFacets$ = this.showFacetsSubject.asObservable();
    this.smallFacetSubject = new BehaviorSubject<boolean>(false);
    this.smallFacet$ = this.smallFacetSubject.asObservable();

    this.resultDisplayMode$ = this._searchStore.pipe(select(fromSearch.getUsedResultViewer));
    // skip initial value
    this.resultDisplayModeSubscription = this.resultDisplayMode$.pipe(skip(1)).subscribe((newMode) => {
      this.changeResultViewMode(newMode);
    });
  }

  allIiifViewers(): string[] {
    return availableIiifViewers(Page.Search, 'ALL');
  }

  resultInViewer(resultDisplayMode): boolean {
    return resultDisplayMode !== ListType.LIST.toString();
  }

  resultAsList(resultDisplayMode): boolean {
    return resultDisplayMode === ListType.LIST.toString();
  }

  displayInUV(resultDisplayMode): boolean {
    return environment.documentViewer[resultDisplayMode].type === ViewerType.UV;
  }

  displayInMirador(resultDisplayMode): boolean {
    return environment.documentViewer[resultDisplayMode].type === ViewerType.MIRADOR;
  }

  ngAfterViewInit() {
    // use bootstrap's breakpoint definitions
    const BP_SMALL = "(max-width: 575.99px)";
    const BP_NORMAL = "(min-width: 576px)";
    this.breakPointSubscription = this.breakpointObserver.observe([
      BP_SMALL,
      BP_NORMAL
    ]).pipe(debounceTime(250)).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[BP_SMALL]) {
          this.smallFacetSubject.next(true);
          this.showFacetsSubject.next(false);
        }
        if (result.breakpoints[BP_NORMAL]) {
          this.smallFacetSubject.next(false);
          const currentResultListMode = this.resultMode();
          this.showFacetsSubject.next(currentResultListMode === ListType.LIST.toString());
        }
      }
    });
    if (this.changeResultViewMode(this.resultMode())) {
      this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
    }
  }

  changeResultViewMode(newMode: string): boolean {
    let found = false;
    let queryChanged = false;

    if (newMode !== ListType.LIST.toString()) {
      const enabledDocViewer = environment.documentViewer[newMode];
      this.showFacetsSubject.next(false);
      const facetRestrictions = enabledDocViewer.facetRestrictions;
      if (facetRestrictions) {
        for (const {field, messageTitleKey, messageTextKey, values} of facetRestrictions) {
          const allowedIds = values.map((fv) => buildId(fv.value));
          let selectedFacets;
          this.selectedFacetValueByKey$.pipe(take(1)).subscribe((v) => selectedFacets = v(field));
          if (selectedFacets) {
            for (const sfv of selectedFacets) {
              if (allowedIds.indexOf(sfv.id) < 0) {
                this._searchStore.dispatch(new fromFormActions.RemoveFacetValue({facet: field, id: sfv.id}));
              } else {
                found = true;
              }
            }
          }
          if (!found) {
            if (messageTextKey) {
              this.toastr.info(messageTextKey, messageTitleKey, {disableTimeOut: true, closeButton: true});
            }
            this._searchStore.dispatch(new fromFormActions.AddFacetValue(
              {facet: field, id: allowedIds[0], label: values[0].label, value: values[0].value}));
            queryChanged = true;
          }
        }
        this._searchStore.dispatch(new fromFormActions.ClearAllInFacetSearch());
      }
    } else {
      // simple list mode
      let smallFacet;
      this.smallFacet$.pipe(take(1)).subscribe(v => smallFacet = v);
      this.showFacetsSubject.next(!smallFacet);
    }
    return queryChanged;
  }

  searchAfterViewModeChange() {
    // iiif search endpoint differs from list based endpoint, so search again is needed
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  protected resultMode() {
    let currentResultListMode: string = defaultViewer();
    this.resultDisplayMode$.subscribe((mode) => currentResultListMode = mode);
    return currentResultListMode;
  }

  findSortField(sortField: string, sortDir: string): SortFieldsModel {
    if (this.sortFields) {
      for (const sortFieldConfig of this.sortFields) {
        if (sortField === sortFieldConfig.field && sortDir === sortFieldConfig.order.toString()) {
          return sortFieldConfig;
        }
      }
    }
    return undefined;
  }

  loadMore() {
    let lastId;
    this.docs$.pipe(take(1)).subscribe((docs) => {
      if (docs && docs.length > 0) {
        lastId = docs[docs.length - 1].search_after_values;
      }
    });
    if (lastId) {
      this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset + this.rowsPerPage));
      this._searchStore.dispatch(new fromQueryActions.NextPage(lastId));
    }
  }

  loadPrevPage() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset - this.rowsPerPage));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  loadNextPage() {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(this.currentOffset + this.rowsPerPage));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  switchSorting(newSorting: SortFieldsModel) {
    this._searchStore.dispatch(new fromQueryActions.SetSortOrder(newSorting.order));
    this._searchStore.dispatch(new fromQueryActions.SetSortField(newSorting.field));
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  changeRowsPerPage(no: number) {
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromQueryActions.SetRows(no));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  ngOnDestroy(): void {
    if (this.offsetSubscription) {
      this.offsetSubscription.unsubscribe();
    }
    if (this.rowsPerPageSubscription) {
      this.rowsPerPageSubscription.unsubscribe();
    }
    if (this.sortSubscriber) {
      this.sortSubscriber.unsubscribe();
    }
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
    }
    if (this.resultDisplayModeSubscription) {
      this.resultDisplayModeSubscription.unsubscribe();
    }
  }

  get env(): SettingsModel {
    return environment;
  }

  toggleFacets() {
    let val;
    this.showFacetsSubject.pipe(take(1)).subscribe(v => val = v);
    this.showFacetsSubject.next(!val);
  }

}
