/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, Input, OnDestroy} from "@angular/core";
import * as fromFormActions from "../actions/form.actions";
import {BehaviorSubject, Observable, Subscription} from "rxjs";
import {select, Store} from "@ngrx/store";
import * as fromSearch from "../reducers";
import * as fromQueryActions from "../actions/query.actions";
import {environment} from "@env/environment";
import {FacetFieldsModel} from "@app/shared/models/settings.model";
import {Actions, ofType} from "@ngrx/effects";

/**
 * Displays simple facet.
 */
@Component({
  selector: 'app-hierarchic-facet',
  template: `
    <ng-template #facetValueTemplate let-key="key" let-value="value" let-selected="selected" let-css="extraCssClass">
      <button
        (click)="facetAction(key, value, selected)"
        type="button"
        class="facet__entry link-black {{css}} {{selected ? 'facet__entry--selected' : ''}}">
        <span class="facet__name">{{value.label}}</span>
        <span class="facet__count"> ({{value.count}})</span>
      </button>
    </ng-template>
    <div class="facet hierarchic-facet">
      <app-operator-selector
        [key]="key"
        (changed)="doSimpleSearch()"
        [facetValueSelector]="fromSearch.getHierarchyValues"></app-operator-selector>
      <app-facet-order-selector
        [key]="key"
        (changed)="doSimpleSearch()"
        [facetValueSelector]="fromSearch.getHierarchyValues"></app-facet-order-selector>
      <ul class="hierarchic-facet__path" *ngIf="(facetFieldByKey$ | async)[key].values.length > 0">
        <li *ngFor="let value of (facetFieldByKey$ | async)[key].values; last as isLast">
          <button *ngIf="!isLast"
                  (click)="replaceFacetValue(key, value)"
                  type="button"
                  class="btn link-black hierarchic-facet__path-element">
            <span class="facet__name">{{'hierarchic-facet.reset_to' | translate}} {{value.label}}</span>
          </button>
          <button *ngIf="isLast"
                  (click)="facetAction(key, value, true)"
                  type="button"
                  class="facet__entry link-black hierarchic-facet__path-element">
            <span class="facet__name">{{'hierarchic-facet.reset_to' | translate}} {{value.label}}</span>
          </button>
        </li>
      </ul>
      <div *ngIf="loading | async" class="rotating_container">
        <div class="rotating"></div>
      </div>
      <ul
        *ngIf="!(loading | async) && facetFieldsConfig[key]"
        class="facet__list facet__list--available"
        [class.first-level]="(facetFieldByKey$ | async)[key].values.length === 0"
      >
        <app-expandable
          [itemTemplate]="facetValueTemplate"
          [itemsPerExpansion]="facetFieldsConfig[key].expandAmount"
          [items]="buildTemplateItems(facetFieldByKey$ | async, facetFieldCountByKey$ | async)"></app-expandable>
      </ul>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HierachicFacetComponent implements OnDestroy {
  @Input() key: string;

  facetFieldByKey$: Observable<any>;
  facetFieldCountByKey$: Observable<any>;

  facetFieldsConfig: FacetFieldsModel;

  loading?: BehaviorSubject<boolean>;
  searchDoneSubscription: Subscription;

  constructor(private _searchStore: Store<fromSearch.State>, actions$: Actions) {
    this.facetFieldByKey$ = _searchStore.pipe(select(fromSearch.getHierarchyValues));
    this.facetFieldCountByKey$ = _searchStore.pipe(select(fromSearch.getHierarchicFacetCountByKey));
    this.facetFieldsConfig = environment.facetFields;
    this.loading = new BehaviorSubject<boolean>(false);
    this.searchDoneSubscription = actions$
      .pipe(ofType(fromQueryActions.QueryActionTypes.SearchSuccess))
      .subscribe(() => this.searchDoneHandler());
  }

  searchDoneHandler() {
    this.loading.next(false);
  }

  facetAction(key, value, selected: boolean) {
    if (selected) {
      this._searchStore.dispatch(new fromFormActions.RemoveHierarchicFacetValue({key: key, id: value.id}));
    } else {
      this._searchStore.dispatch(new fromFormActions.AddHierarchicFacetValue({
        key: key, id: value.id, label: value.label, value: value.value
      }));
    }
    this.doSimpleSearch();
  }

  buildTemplateItems(facetFieldByKey, facetFieldCountByKey) {
    const facet = facetFieldByKey[this.key];
    let values = facetFieldCountByKey(facet.field);
    if (!values) {
      values = [];
    }
    let cssClass = "";
    if (facet.values.length === 0) {
      cssClass = "hierarchic-facet__root-element"
    }
    // don't show already selected facet values
    return values.map((v) => ({key: this.key, value: v, selected: false, extraCssClass: cssClass}));
  }

  protected isSelected(facet, id): boolean {
    const selected = facet.values.filter((f) => f.id === id);
    return selected.length !== 0;
  }

  replaceFacetValue(key, value) {
    this._searchStore.dispatch(new fromFormActions.ResetHierarchicFacetValueTo({key: key, id: value.id}));
    this.doSimpleSearch();
  }

  doSimpleSearch() {
    this.loading.next(true);
    this._searchStore.dispatch(new fromQueryActions.SetOffset(0));
    this._searchStore.dispatch(new fromQueryActions.SimpleSearch());
  }

  get fromSearch() {
    return fromSearch;
  }

  ngOnDestroy(): void {
    if (this.searchDoneSubscription) {
      this.searchDoneSubscription.unsubscribe();
    }
  }
}
