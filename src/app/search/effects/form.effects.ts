/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {FormActionTypes, RangeBoundariesChanged, UpdateRangeBoundaries} from '../actions/form.actions';
import {debounceTime, map} from 'rxjs/operators';


/**
 * Provides effects for changes in search form parameters
 */
@Injectable()
export class FormEffects {

  /**
   * Fires a {@link UpdateRangeBoundaries} action after a delay when a
   * {@link RangeBoundariesChanged} action was triggered
   */
  @Effect()
  rangeBoundariesChanged$ = this.actions$.pipe(
    ofType(FormActionTypes.RangeBoundariesChanged),
    debounceTime(250),
    map((action: RangeBoundariesChanged) => new UpdateRangeBoundaries(action.payload))
  );

  /**
   * @ignore
   */
  constructor(private actions$: Actions) {
  }
}
