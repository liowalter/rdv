/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy} from "@angular/core";
import {Store} from '@ngrx/store';

import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import {environment} from '@env/environment';
import * as fromRoot from '../../reducers';
import * as fromActions from '../actions/remote-filter-configs.actions';
import {HeaderSettingsModel, headerSettingsModelByLanguage} from "@app/shared/models/settings.model";
import {Subscription} from "rxjs/internal/Subscription";
import {BehaviorSubject, Observable} from "rxjs";
import {Title} from "@angular/platform-browser";

/**
 * Main entry point to the application.
 */
@Component({
  selector: 'app-root',
  template: `
    <app-beta-header *ngIf="(headerSettings$ | async)?.showBetaBar"
                     contactName="top.headerSettings.betaBarContact.name"
                     contactEmail="top.headerSettings.betaBarContact.email"
    ></app-beta-header>
    <app-header></app-header>
    <router-outlet class="no-gutters"></router-outlet>
    <app-footer></app-footer>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AppComponent implements OnDestroy, AfterViewInit {
  languageChangeSubscription: Subscription;
  headerSettingsSubject: BehaviorSubject<HeaderSettingsModel>;
  headerSettings$: Observable<HeaderSettingsModel>;

  /**
   * @ignore
   */
  constructor(private _rootStore: Store<fromRoot.State>, protected translate: TranslateService, protected  titleService: Title) {
    this._fetchRemoteFilterConfigs();
    this.headerSettingsSubject = new BehaviorSubject(undefined);
    translate.setDefaultLang(environment.defaultLanguage || 'de');
    this.languageChanged(translate.currentLang);
    this.headerSettings$ = this.headerSettingsSubject.asObservable();
    this.setTitle();
  }

  protected setTitle() {
    this.titleService.setTitle(this.translate.instant(this.envName(this.headerSettingsSubject.value)));
  }

  /**
   * Triggers remote fetching of filter options if respective filter value is an URL
   */
  private _fetchRemoteFilterConfigs() {
    for (const key of Object.keys(environment.filterFields)) {
      if (environment.filterFields[key].url) {
        this._rootStore.dispatch(
          new fromActions.GetRemoteFilterFieldOptions({key: key, url: environment.filterFields[key].url})
        );
      }
    }
  }

  ngOnDestroy(): void {
    if (this.languageChangeSubscription) {
      this.languageChangeSubscription.unsubscribe();
    }
  }

  protected languageChanged(language) {
    const headerSettings = headerSettingsModelByLanguage(language, environment.headerSettings);
    this.headerSettingsSubject.next(headerSettings);
    this.setTitle();
  }

  ngAfterViewInit(): void {
    this.languageChangeSubscription = this.translate.onLangChange.subscribe(
      (params: LangChangeEvent) => this.languageChanged(params.lang));
  }

  envName(headerSettingsModel: HeaderSettingsModel): string {
    return (headerSettingsModel && headerSettingsModel.name || "top.headerSettings.name")
  }
}
