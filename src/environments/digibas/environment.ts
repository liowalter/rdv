/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";
import {viewer} from '@env_temp/viewer';

export const environment: SettingsModel = {
  ...viewer.documentViewer["UV"].facetRestrictions = [],
  ...viewer.documentViewer["Mirador"].facetRestrictions = [],
  ...viewer.documentViewer["UVScroll"].facetRestrictions = [],
  ...viewer.documentViewer["MiradorSinglePage"].facetRestrictions = [],
  production: false,
  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-digibas.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },


  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "all_text": "Freitext",
      "fct_location.search": "Geographikum",
      "fct_mediatype.search": "Datentr\u00e4ger/Inhaltstyp",
      "fct_person_organisation.search": "Person/Institution",
      "fct_topic.search": "Schlagwort",
      "field_title": "Titel/Benennung"
    },
    "preselect": [
      "all_text"
    ]
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {
  },

//Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
//order gilt fuer Facetten und Ranges
  facetFields: {
    "kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "hierarchy_filter.keyword": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Struktur Bildersammlung Jacob Burckhardt",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 10
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "signatur": {
      "field": "signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "Sprache": {
      "field": "gen.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "Ebene": {
      "field": "level.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ebene",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "GenreForm": {
      "field": "genreform.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Genre/Form",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "Localcode": {
      "field": "localcode.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Localcode",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "Ort": {
      "field": "place.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "FUV": {
      "field": "fuv.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "FUV",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "STW": {
      "field": "stw_ids.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "STW",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "rel_persons2": {
      "field": "rel_persons2",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Rel Person Typ",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "rel_persons2 Auswahl": {
      "field": "rel_persons2.Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Rel Person Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
  },

// @Depreacted: only used in "old" search form, for new search add to "facetFields"
//Infos zu Ranges (z.B. Label)
//order gilt fuer Facetten und Ranges
  rangeFields: {
  },

//Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "fct_date",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "fct_date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

//Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_score",
      "sortOrder": SortOrder.DESC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

//Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
  ],

//Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
    },
    "fct_mediatype": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_mediatype",
      "label": "Datentr\u00e4ger/Inhaltstyp"
    },
    "fct_topic": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_topic",
      "label": "Schlagwort"
    },
    "field_date_input_values": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "field_date_input_values",
      "label": "Zeit (Ausgangswert)"
    }
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "DigiBasel",
      "top.headerSettings.name.Dev": "DigiBasel (Dev)",
      "top.headerSettings.name.Loc": "DigiBasel (Loc)",
      "top.headerSettings.name.Test": "DigiBasel (Test)",
    },
    "en": {
      "top.headerSettings.name": "DigiBasel",
      "top.headerSettings.name.Dev": "DigiBasel (Dev)",
      "top.headerSettings.name.Loc": "DigiBasel (Loc)",
      "top.headerSettings.name.Test": "DigiBasel (Test)",
    }
  },
};
