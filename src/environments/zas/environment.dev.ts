import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "zas-dev";

import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/zas/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...dev,
  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: undefined
};
