import {environment as dev} from "@env_temp/environment.type-dev";

const Proj = "afrikaportal";

import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/afrikaportal/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,

  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: prod.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: prod.suggestSearchWordProxyUrl +  Proj + "/",
};
