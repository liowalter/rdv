/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {

  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-ap.ub.unibas.ch/afrikaportal_v6",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}},
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}},
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}},
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}},
          ]
        }
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "hidden_digitized",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "search-results.view-mode.UV.object_type.message.text",
          values: [
            {label: "ja", value: {"id": "ja"}},
          ]
        }
      ],
    }
  },


  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "all_text": "Freitext",
      "fct_location.search": "Geographikum",
      "fct_mediatype.search": "Datentr\u00e4ger/Inhaltstyp",
      "fct_person_organisation.search": "Person/Institution",
      "fct_topic.search": "Schlagwort",
      "field_title": "Titel/Benennung"
    },
    "preselect": [
      "all_text"
    ]
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

//Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
//order gilt fuer Facetten und Ranges
  facetFields: {
    "fct_date": {
      "field": "fct_date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "fct_countryname": {
      "field": "fct_countryname.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Land",
      "operator": "OR",
      "order": 7,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_institution": {
      "field": "fct_institution.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sammlung/Datengeber/Standort",
      "operator": "OR",
      "order": 3,
      "expandAmount": 10,
    },
    "fct_location": {
      "field": "fct_location.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Geographikum",
      "operator": "OR",
      "order": 2,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "hidden_digitized": {
      "field": "hidden_digitized.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "order": 2,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_mediatype": {
      "field": "fct_mediatype.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datentr\u00e4ger/Inhaltstyp",
      "operator": "OR",
      "order": 6,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_person_organisation": {
      "field": "fct_person_organisation.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Person/Institution",
      "operator": "OR",
      "order": 4,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "fct_topic": {
      "field": "fct_topic.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Schlagwort",
      "operator": "OR",
      "order": 5,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "hierarchy_filter.keyword": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "BAB Thesaurus",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 10
    }
  },

// @Depreacted: only used in "old" search form, for new search add to "facetFields"
//Infos zu Ranges (z.B. Label)
//order gilt fuer Facetten und Ranges
  rangeFields: {},

//Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "fct_date",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "fct_date",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

//Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_score",
      "sortOrder": SortOrder.DESC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

//Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

//Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
    },
    "fct_mediatype": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_mediatype",
      "label": "Datentr\u00e4ger/Inhaltstyp"
    },
    "fct_topic": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_topic",
      "label": "Schlagwort"
    },
    "field_date_input_values": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "field_date_input_values",
      "label": "Zeit (Ausgangswert)"
    }
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Afrikaportal",
      "top.headerSettings.name.Dev": "Afrikaportal (Dev)",
      "top.headerSettings.name.Loc": "Afrikaportal (Loc)",
      "top.headerSettings.name.Test": "Afrikaportal (Test)",
    },
    "en": {
      "top.headerSettings.name": "africaportal",
      "top.headerSettings.name.Dev": "africaportal (Dev)",
      "top.headerSettings.name.Loc": "africaportal (Loc)",
      "top.headerSettings.name.Test": "africaportal (Test)",
    }
  },
};
