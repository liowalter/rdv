import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

import {environment as zas} from '@env/zas/environment';
export const environment: SettingsModel = {
  ...zas,
  baseUrl: "https://ub-zas_int.ub.unibas.ch/",
  editable: true,
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  embeddedIiifViewer: "right",
  narrowEmbeddedIiiFViewer: false,
  facetFields: {
    ...zas.facetFields,
    "Farbcode": {
      "field": "Farbcode.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bearbeitungscode",
      "operator": "OR",
      "order": 0,
      "size": 100,
      "expandAmount": 10
    } ,
    "Systemnummer": {
      "field": "Systemnummer.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Systemnummer",
      "operator": "OR",
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "Export": {
      "field": "Export.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Export",
      "operator": "OR",
      "order": 13,
      "size": 100,
      "expandAmount": 10,
      "hidden": true
    },
    "ki_descr_sach_prefLabel": {
      "field": "ki_stw_ids.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen KI (experimentell)",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 20,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "hidden": {
      "field": "hidden",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausblenden",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Rechercheportal Zeitungsausschnitte intern",
      "top.headerSettings.name.Dev": "Rechercheportal Zeitungsausschnitte intern (Dev)",
      "top.headerSettings.name.Loc": "Rechercheportal Zeitungsausschnitte intern (Loc)",
      "top.headerSettings.name.Test": "Rechercheportal Zeitungsausschnitte intern (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    },
    "en": {
      "top.headerSettings.name": "Search Portal Press Clippings intern",
      "top.headerSettings.name.Dev": "Search Portal Press Clippings intern (Dev)",
      "top.headerSettings.name.Loc": "Search Portal Press Clippings intern (Loc)",
      "top.headerSettings.name.Test": "Search Portal Press Clippings intern (Test)",
      "top.headerSettings.betaBarContact.name": "UB Wirtschaft - SWA",
      "top.headerSettings.betaBarContact.email": "info-ubw-swa@unibas.ch",
    }
  },

};
