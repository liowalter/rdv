const Proj = "bla-test";
import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/bla/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...test,
  headerSettings: addTestNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: test.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: test.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-test-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};
