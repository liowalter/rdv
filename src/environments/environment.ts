/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Backend, ExtraInfoFieldType, SettingsModel, SortOrder} from "../app/shared/models/settings.model";

export const environment: SettingsModel = {

  production: false,

  backend: Backend.ELASTIC,

  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://localhost:4200",

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "http://afrikaportal:UbBaSeL@ub-afrikaportal.ub.unibas.ch/unibas/php-proxy/angularx_elasticsearch_proxy_unibas.php",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "all_text": "Freitext",
      "Titel": "Titel",
      "fct_location.search": "Ort",
      "Verlag": "Verlag"
    },
    "preselect": ["all_text", "Titel", "Verlag"]
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {
    "institution": {
      "label": "Ort",
      "field": "hidden_analysis_fct_location",
      "options": [
        {
          "value": "Ort",
          "label": "Ort"
        },
        {
          "value": "Druckort",
          "label": "Druckort"
        },
        {
          "value": "Aushangsort",
          "label": "Aushangsort"
        }
      ]
    },
    "type": {
      "label": "Typ",
      "field": "fct_type",
      "options": [
        {
          "value": "Apartheid",
          "label": "Apartheid"
        },
        {
          "value": "Kunst",
          "label": "Kunst"
        },
        {
          "value": "Widerstand",
          "label": "Widerstand"
        }
      ]
    }
  },

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "topic": {
      "field": "fct_topic",
      "label": "Topic",
      "operators": ["OR", "AND"],
      "operator": "AND",
      "order": 2
    },
    "country": {
      "field": "fct_countrycode",
      "label": "Country",
      "operators": ["OR"],
      "operator": "OR",
      "order": 3
    }
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "year": {
      "field": "fct_year",
      "label": "Jahr",
      "order": 1,
      "min": 1950,
      "from": 1950,
      "to": 2018,
      "max": 2018,
      "showMissingValues": true

    },
  },

//Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  queryParams: {
    "rows": 10, // 5, 10, 20, 50
    "offset": 0,
    "sortField": "_uid",
    "sortOrder": SortOrder.ASC,
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_uid",
      "sortOrder": SortOrder.ASC,
    }
  },

  showExportList: {
    "basket": true,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "field": "id",
      "label": "ID",
      "sort": "_uid",
      "css": "col-sm-2 col-lg-1 flex-sm-column align-items-center text-sm-center",
      "extraInfo": true,
      "landingpage": true,
    },
    {
      "field": "hidden_analysis_fct_location",
      "label": "Region",
      "sort": "hidden_analysis_fct_location",
      "css": "col-sm-2 col-lg-2 text-left",
    },
    {
      "field": "Titel",
      "label": "Titel",
      "sort": "Titel.keyword",
      "css": "col-sm-5 col-lg-6 text-left",
    },
    {
      "field": "fct_year",
      "label": "Jahr",
      "sort": "fct_year",
      "css": "col-sm-2 col-lg-2 text-left",
    }

  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "date": {
      "field": "Anfangsdatum",
      "label": "Anfangsdatum",
      "display": ExtraInfoFieldType.TEXT
    },
    "number": {
      "field": "Inventarnummer",
      "label": "Inventarnummer",
      "display": ExtraInfoFieldType.TEXT
    }
  },
};
