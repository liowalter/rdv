import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-azdb.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: true,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "title.label.keyword",
      order: SortOrder.ASC,
      display: "select-sort-title.asc"
    },
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 10,
    },
    "Titel": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "Zeitungstyp": {
      "field": "newspaper_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zeitung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
    },
    "DBtyp": {
      "field": "db_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenbank",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
    },
    "fachgebiet": {
      "field": "fachgebiet.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenbank Fachgebiet",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "dbsubject": {
      "field": "subject.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenbank Thema",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "access": {
      "field": "access.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datenbank Zugriff",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10,
    },
    "Code": {
      "field": "code.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Code",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "Erscheinungsrythmus": {
      "field": "rythm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zeitung Erscheinungsrythmus",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 10,
    },
    "Erscheinungsort": {
      "field": "erscheinungsort.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zeitung Erscheinungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3,
    },
    "test": {
      "field": "test.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "test",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "test2": {
      "field": "test2.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "test2",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 13,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "Land": {
      "field": "country.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Land",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "title.label.keyword",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "select-sort-title.asc": "Titel",
      "top.headerSettings.name": "Datenbanken",
      "top.headerSettings.name.Dev": "Datenbanken (Dev)",
      "top.headerSettings.name.Loc": "Datenbanken (Loc)",
      "top.headerSettings.name.Test": "Datenbanken (Test)",
    },
    "en": {
      "top.headerSettings.name": "Datenbanken",
      "top.headerSettings.name.Dev": "Datenbanken (Dev)",
      "top.headerSettings.name.Loc": "Datenbanken (Loc)",
      "top.headerSettings.name.Test": "Datenbanken (Test)",
    }
  },

};
