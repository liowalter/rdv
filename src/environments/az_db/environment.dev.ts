import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "az_db-dev";
import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/az_db/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...dev,
  headerSettings: addDevNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl +  Proj + "/",
};
