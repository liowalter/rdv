const Proj = "edoc-test";
import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/edoc/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...test,
  headerSettings: addTestNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : test.proxyUrl +  Proj + "/",
  moreProxyUrl: test.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: test.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: test.suggestSearchWordProxyUrl +  Proj + "/",
};
