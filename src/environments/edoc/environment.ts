import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-edocrdv.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "hierarchy_filter.keyword",
      order: SortOrder.ASC,
      display: "select-sort-hier.asc"
    },
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "dateAdded",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "main_type": {
      "field": "main_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 10,
      "autocomplete_size": 3
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Uni-Hierarchie",
      "operator": "AND",
      "order": 1,
      "size": 100,
      "expandAmount": 100,
    },
    "divisions_ids": {
      "field": "divisions_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fakultät / Departement",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "title": {
      "field": "title.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Typ",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "typology_edoc_id": {
      "field": "typology_edoc_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "spez. Objekttyp",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "date": {
      "field": "date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Publikationsdatum",
      "operator": "AND",
      "showAggs": true,
      "order": 13,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,

    "creators": {
      "field": "creators.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Creator",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "contributors": {
      "field": "contributors.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Contributor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "submitters": {
      "field": "submitters.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Submitter",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 22,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "publisher": {
      "field": "publisher.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Publisher",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 23,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true
    },
    "investigator": {
      "field": "investigator.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Investigator",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 24,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "coinvestigator": {
      "field": "coinvestigator.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Coinvestigator",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 25,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "member": {
      "field": "member.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Member",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 26,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "submitter": {
      "field": "submitter.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Submitter Projekt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },

    "eprint_status": {
      "field": "eprint_status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Pub Status",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 30,
      "size": 100,
      "expandAmount": 10,
    },
    "full_text_status": {
      "field": "full_text_status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Volltext Status",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 31,
      "size": 100,
      "expandAmount": 10,
    },
    "project_status": {
      "field": "project_status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Projekt Status",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 32,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },




    "eprintid": {
      "field": "eprintid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Eprint Id",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "mcss_id": {
      "field": "mcss_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "mcss Id",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 51,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "id_number2": {
      "field": "id_number.type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Id Type",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 52,
      "size": 100,
      "expandAmount": 10,
    },
    "id_number": {
      "field": "id_number.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "externe ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 53,
      "size": 100,
      "expandAmount": 10,
    },

    "status_changed": {
      "field": "status_changed",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Statuswechsel",
      "operator": "AND",
      "showAggs": true,
      "order": 90,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "datestamp": {
      "field": "lastmod",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Lastmod",
      "operator": "AND",
      "showAggs": true,
      "order": 91,
      "size": 100,
      "expandAmount": 31
    } as HistogramFieldModel,


  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "top.headerSettings.name": "edoc RDV",
      "top.headerSettings.name.Dev": "edoc RDV (Dev)",
      "top.headerSettings.name.Loc": "edoc RDV (Loc)",
      "top.headerSettings.name.Test": "edoc RDV (Test)",
      "top.headerSettings.betaBarContact.name": "Open Access",
      "top.headerSettings.betaBarContact.email": "openaccess@unibas.ch",
    },
    "en": {
      "top.headerSettings.name": "edoc RDV",
      "top.headerSettings.name.Dev": "edoc RDV (Dev)",
      "top.headerSettings.name.Loc": "edoc RDV (Loc)",
      "top.headerSettings.name.Test": "edoc RDV (Test)",
      "top.headerSettings.betaBarContact.name": "Open Access",
      "top.headerSettings.betaBarContact.email": "openaccess@unibas.ch",
    }
  },

};
