import {environment as dev} from "@env_temp/environment.type-dev";

const Proj = "bestandsdb-loc";

import {SettingsModel} from "@app/shared/models/settings.model";
import {environment as proj} from "@env/bestandsdb/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...loc,
  headerSettings: addLocNamePostfix(proj.headerSettings),

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: loc.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: loc.detailSuggestionProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-loc-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};
