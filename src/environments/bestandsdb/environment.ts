/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {

  // Defaultsprache für fehlende Übersetzungen in anderen Sprachen
  defaultLanguage: 'de',

  // optional pro Sprache environment-spezifische Übersetzungen
  // i18n: {
  //   "de": {
  //     "beta-header.contact": "XXX Kontaktieren Sie bei Fragen: ",
  //   },
  //   "en": {
  //     "beta-header.contact": "YYY Contact: ",
  //   }
  // },

  production: false,

  showSimpleSearch: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-bestandsb.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UVScroll': {
      enabled: false,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "object_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
            {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
            {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: false,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  editable: false,
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/",

  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",
  externalHelpUrl: "https://ub.unibas.ch/de/historische-bestaende/wirtschaftsdokumentation/zas-portal-infos/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "Quelldatum",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Datum.Eingang": {
      "field": "Datum.Eingang",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum Eingang",
      "operator": "AND",
      "showAggs": true,
      "order": 80,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Datum.Ausgang": {
      "field": "Datum.Ausgang",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum Ausgang",
      "operator": "AND",
      "showAggs": true,
      "order": 82,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Datum Konservierung": {
      "field": "Dokumentation Konservierung.am",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum Konservierung",
      "operator": "AND",
      "showAggs": true,
      "order": 81,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Signatur": {
      "field": "Signatur.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Signatur",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "help": "env.facetFields.Signatur.help",
      "autocomplete_size": 3
    },
    "Anzahl Signaturen / Signatur": {
      "field": "Anzahl Signaturen / Signatur",
      "label": "Anzahl Signaturen / Signatur",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 100,
      "help": "env.facetFields.Anzahl Signatur.help",
      "expandAmount": 10
    } as HistogramFieldModel,
    "Status": {
      "field": "Status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Status",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "BearbeiterIn": {
      "field": "BearbeiterIn.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "BearbeiterIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 82,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "BestellerIn": {
      "field": "BestellerIn.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "BestellerIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 83,
      "expandAmount": 10,
      "size": 100,
    },
    "Dokumentation Konservierung": {
      "field": "Dokumentation Konservierung",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Dokumentation Konservierung Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.Dokumentation Konservierung.help",
    },
    "Dokumentation Konservierung Anzahl": {
      "field": "Dokumentation Konservierung.BP",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Dokumentation Konservierung Anzahl",
      "operator": "AND",
      "showAggs": true,
      "order": 21,
      "size": 100,
      "help": "env.facetFields.Dokumentation Konservierung Anzahl.help",
      "expandAmount": 10
    } as HistogramFieldModel,
    "Digi - Gerät": {
      "field": "Digi - Gerät.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digi - Gerät",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 71,
      "size": 100,
      "expandAmount": 10,
    },
    "Varia": {
      "field": "Varia.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Varia",
      "operator": "OR",
      "operators": [
        "OR", "AND"
      ],
      "order": 6,
      "size": 100,
      "help": "env.facetFields.Varia.help",
      "expandAmount": 10,
    },
    "Gesperrt": {
      "field": "Gesperrt.null",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Gesperrt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 6,
      "size": 100,
      "expandAmount": 10,
    },
    "Zeitraum": {
      "field": "Zeitraum.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zeitraum",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
    },
    "Jahr": {
      "field": "jahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Jahr",
      "operator": "AND",
      "showAggs": true,
      "order": 2,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,
    "Quelle": {
      "field": "Quelle.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Quelle",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 5,
      "size": 100,
      "expandAmount": 10
    },
    "Projektname Digi-Projekt": {
      "field": "Projektname Digi-Projekt.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Projektname Digi-Projekt",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 70,
      "size": 100,
      "expandAmount": 10
    },
    "Öffnungswinkel": {
      "field": "Öffnungswinkel.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Öffnungswinkel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 72,
      "size": 100,
      "expandAmount": 10,
    },
    "Schadenskategorie": {
      "field": "Schadenskategorie.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Schadenskategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 5,
      "help": "env.facetFields.Schadenskategorie.help",
    },
    "Tintenfrass": {
      "field": "Tintenfrass.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Tintenfrass",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 5,
    },
    "Schäden/Besonderheiten": {
      "field": "Schäden/Besonderheiten.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Schäden/Besonderheiten",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "Ingest-Tag": {
      "field": "tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ingest-Tag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "help": "env.facetFields.ingest.help"
    },
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "help": "env.facetFields.digitalisiert.help"
    },
    "Systemnummer": {
      "field": "sys_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Systemnummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Bestandsgruppe DSV05",
      "operator": "AND",
      "order": 0,
      "size": 100,
      "expandAmount": 100,
      "help": "env.facetFields.hierarchy_filter.help",
    },
    "Bestand": {
      "field": "bestand.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bestand",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
    },
    "fields_300": {
      "field": "fields_300.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Marc 300",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "help": "env.facetFields.fields_300.help",
    }
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 50,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  },

  i18n: {
    "de": {
      "env.facetFields.hierarchy_filter.help": "Hierarchischer Filter um die Bestandsgruppen einzugrenzen",
      "env.facetFields.ingest.help": "Angaben, ob beim Ingest ein eindeutiger Abgleich mit dem Katalog über die Signatur erfolgen konnte",
      "env.facetFields.digitalisiert.help": "Digitalisate von den e-Plattformen können dank IIIF eingebunden werden (siehe Viewer auf den Detailseiten)",
      "env.facetFields.fields_300.help": "Beispiel dafür, wie auch Daten aus dem Katalog für die Suche integriert werden können.",
      "env.facetFields.Signatur.help": "für Facetten kann eine Suche in den Werten aktiviert werden. " +
        "Momentan handelt es sich dabei nicht um eine Indexsuche. Das könnte aber angepasst werden",
      "env.facetFields.Varia.help": "In dieser Facette wurden diverse Spalten mit wenigen Werten zusammengefasst, " +
        "daher kann auch eine 'AND' Verknüpfung gewählt werden",
      "env.facetFields.Dokumentation Konservierung.help": "Hier kann definiert werden in welchem " +
        "Feld die Facette 'Dokumentation Konservierung Anzahl' sucht." +
        "Das ist eine proofOfConcept Umsetzung. Falls diese Funktionalität benötigt wird, kann das besser dargestellt werden.",
      "env.facetFields.Dokumentation Konservierung Anzahl.help": "Siehe Facette Auswahl",
      "env.facetFields.Anzahl Signatur.help": "Für Range-Facetten (Zahlen/Datum) können auch Gruppierungen und Anzahl eingeblendet werden",
      "env.facetFields.Schadenskategorie.help": "Beispieltext z.b. um externen Nutzern die BestDB näher zu bringen: Die Schadenskategorie definiert ...",
      "top.headerSettings.name": "Bestandsdatenbank",
      "top.headerSettings.name.Dev": "Bestandsdatenbank (Dev)",
      "top.headerSettings.name.Loc": "Bestandsdatenbank (Loc)",
      "top.headerSettings.name.Test": "Bestandsdatenbank (Test)",
      "top.headerSettings.betaBarContact.name": "Bestandeserhaltung",
      "top.headerSettings.betaBarContact.email": "ub-bubi@unibas.ch",
    }
  },

};
