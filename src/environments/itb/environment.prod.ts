import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "itb";

import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/itb/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,
  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: prod.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: prod.detailSuggestionProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
};
