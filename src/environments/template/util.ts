import {HeaderSettings} from "@app/shared/models/settings.model";

const i18nBaseKeyName = "top.headerSettings.name";

// aot/prod build complains: "Function calls are not supported in decorators"
// only macros are allowed (functions returning an expression)
// see https://angular.io/guide/aot-compiler
export function addDevNamePostfix(baseHeaderSettings: HeaderSettings): HeaderSettings {
  return {
    ...baseHeaderSettings,
    default: {
      ...baseHeaderSettings.default,
      name: (baseHeaderSettings.default.name ? baseHeaderSettings.default.name : i18nBaseKeyName) + ".Dev"
    }
  };
}

export function addLocNamePostfix(baseHeaderSettings: HeaderSettings): HeaderSettings {
  return {
    ...baseHeaderSettings, default: {
      ...baseHeaderSettings.default,
      name: (baseHeaderSettings.default.name ? baseHeaderSettings.default.name : i18nBaseKeyName) + ".Loc"
    }
  };
}

export function addTestNamePostfix(baseHeaderSettings: HeaderSettings): HeaderSettings {
  return {
    ...baseHeaderSettings, default: {
      ...baseHeaderSettings.default,
      name: (baseHeaderSettings.default.name ? baseHeaderSettings.default.name : i18nBaseKeyName) + ".Test"
    }
  };
}
