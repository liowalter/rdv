// sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
// Auswahlmöglichkeit gegeben
import {Page, ViewerType} from "@app/shared/models/settings.model";

export const viewer = {
documentViewer: {
  // [ViewerType.MIRADOR]: { // not working if aot/prod is built
  'Mirador': {
    enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'Mirador-3.0.0-rc.5': {
    enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  // [ViewerType.UV]: { // not working if aot/prod is built
  'UV': {
    enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
        messageTextKey: "search-results.view-mode.UV.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'UV-3.0.36': {
    enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}, {view: Page.Detail, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.UV-3.0.36.object_type.message.title",
        messageTextKey: "search-results.view-mode.UV-3.0.36.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'UVScroll': {
    enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
        messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'UV-3.0.36-Scroll': {
    enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}], // default: on all views
      viewerUrl: "assets/uv-3.0.36/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.UV-3.0.36-Scroll.object_type.message.title",
        messageTextKey: "search-results.view-mode.UV-3.0.36-Scroll.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'MiradorScroll': {
    enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'Mirador-3.0.0-rc.5-Scroll': {
    enabled: false,
      enabledForPage: [{view: Page.Search, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'MiradorSinglePage': {
    enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  },
  'Mirador-3.0.0-rc.5-SinglePage': {
    enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador-3.0.0-rc.5/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      {
        field: "object_type",
        messageTitleKey: "search-results.view-mode.Mirador.object_type.message.title",
        messageTextKey: "search-results.view-mode.Mirador.object_type.message.text",
        values: [
          {label: "Zeitungsausschnitt", value: {"id": "Zeitungsausschnitt"}},
          {label: "retrodigitalisierter Zeitungsausschnitt", value: {"id": "retrodigitalisierter Zeitungsausschnitt"}},
          {label: "elektronischer Zeitungsausschnitt (ab 2013)", value: {"id": "elektronischer Zeitungsausschnitt (ab 2013)"}}
        ]
      }
    ],
  }
}
}
