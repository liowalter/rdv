import {BaseSettings} from "@app/shared/models/settings.model";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: false,

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "http://127.0.0.1:5000/v1/rdv_query/es_proxy/",
  moreProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/facet_search/",
  detailProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_view/",
  documentViewerProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/iiif_flex_pres/",
  navDetailProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/next_objectview/",
  popupQueryProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/popup_query/",
  detailSuggestionProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/autocomplete/",
};
