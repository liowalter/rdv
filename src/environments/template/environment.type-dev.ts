import {BaseSettings} from "@app/shared/models/settings.model";
import {viewer} from '@env_temp/viewer';
import {richtextEditor} from "@env_temp/richtexteditor";

export const environment: BaseSettings = {
  ...viewer,
  ...richtextEditor,
  production: false,

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/es_proxy/",
  moreProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/facet_search/",
  popupQueryProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/popup_query/",
  documentViewerProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/iiif_flex_pres/",
  detailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_object/object_view/",
  navDetailProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/next_objectview/",
  detailSuggestionProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/form_query/",
  suggestSearchWordProxyUrl: "https://ub-rdv-dev-proxy.ub.unibas.ch/v1/rdv_query/autocomplete/",
};
