import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-bibnetz.ub.unibas.ch/",
  editable: true,

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: true,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: false
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "Bib-Name.keyword",
      order: SortOrder.ASC,
      display: "select-sort-fields.name_label_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Bib-Name": {
      "field": "Bib-Name.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bib-Name",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "autocomplete_size": 3
    },
    "Fachgebiet": {
      "field": "Fachgebiet.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Fachgebiet",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Benutzung": {
      "field": "benutzung.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Benutzung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "ausleihe": {
      "field": "ausleihe.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ausleihe",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "online-katalog": {
      "field": "online-katalog.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Online-Katalog",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "öffnungszeiten": {
      "field": "öffnungszeiten.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Öffnungszeiten",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 15,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "Bib-Name.keyword",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "select-sort-fields.name_label_desc": "Bib-Name",
      "top.headerSettings.name": "Bibliotheksnetz",
      "top.headerSettings.name.Dev": "Bibliotheksnetz (Dev)",
      "top.headerSettings.name.Loc": "Bibliotheksnetz (Loc)",
      "top.headerSettings.name.Test": "Bibliotheksnetz (Test)",
    },
    "en": {
      "select-sort-fields.name_label_desc": "Bib-Name",
      "top.headerSettings.name": "Bibliotheksnetz",
      "top.headerSettings.name.Dev": "Bibliotheksnetz (Dev)",
      "top.headerSettings.name.Loc": "Bibliotheksnetz (Loc)",
      "top.headerSettings.name.Test": "Bibliotheksnetz (Test)",
    }
  },

};
