/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  SettingsModel,
  SortOrder,
  FacetFieldType,
  HistogramFieldModel, HistogramFieldType
} from '../app/shared/models/settings.model';

export const environment: SettingsModel = {

  production: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-afrikaportal.ub.unibas.ch/afrikaportal_v6",

  //Wo liegt Proxy-Skript, das mit Elasticsearch spricht?
  proxyUrl : "http://127.0.0.1:5000/v1/rdv_query/es_proxy/" ,
  moreProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/further_snippets/",
  inFacetSearchProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/facet_search/",

  detailProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_view/",
  navDetailProxyUrl: "http://127.0.0.1:5000/v1/rdv_query/next_objectview/",
  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "all_text": "Freitext",
      "fct_location.search": "Geographikum",
      "fct_mediatype.search": "Datentr\u00e4ger/Inhaltstyp",
      "fct_person_organisation.search": "Person/Institution",
      "fct_topic.search": "Schlagwort",
      "field_title": "Titel/Benennung"
    },
    "preselect": [
      "all_text"
    ]
  },

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {
  },

//Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
//order gilt fuer Facetten und Ranges
  facetFields: {
    "fct_date": {
      "field": "fct_date",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 0,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "fct_countryname": {
      "field": "fct_countryname",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Land",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 7,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_institution": {
      "field": "fct_institution",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sammlung/Datengeber/Standort",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 3,
      "expandAmount": 10,
    },
    "fct_location": {
      "field": "fct_location",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Geographikum",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 2,
      "expandAmount": 10,
      "searchWithin": true
    },
    "hidden_digitized": {
      "field": "hidden_digitized",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 2,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_mediatype": {
      "field": "fct_mediatype",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datentr\u00e4ger/Inhaltstyp",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 6,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_person_organisation": {
      "field": "fct_person_organisation",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Person/Institution",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 4,
      "expandAmount": 10,
      "searchWithin": true
    },
    "fct_topic": {
      "field": "fct_topic",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Schlagwort",
      "operator": "OR",
      "operators": [
        "OR",
        "AND"
      ],
      "order": 5,
      "expandAmount": 10,
      "searchWithin": true
    },
    "hierarchy_filter.keyword": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "BAB Thesaurus",
      "operator": "AND",
      "order": 4,
      "size": 100,
      "expandAmount": 10
    },
  },

// @Depreacted: only used in "old" search form, for new search add to "facetFields"
//Infos zu Ranges (z.B. Label)
//order gilt fuer Facetten und Ranges
  rangeFields: {
  },

//Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "_uid",
    "sortOrder": SortOrder.ASC
  },

//Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_uid",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": true,
    "table": false
  },

//Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
  ],

//Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
    },
    "fct_mediatype": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_mediatype",
      "label": "Datentr\u00e4ger/Inhaltstyp"
    },
    "fct_topic": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_topic",
      "label": "Schlagwort"
    },
    "field_date_input_values": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "field_date_input_values",
      "label": "Zeit (Ausgangswert)"
    }
  },
};
