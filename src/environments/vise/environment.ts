import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: true,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-vise.ub.unibas.ch/",

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: true,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "hierarchy_filter.keyword",
      order: SortOrder.ASC,
      display: "select-sort-hier.asc"
    },
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "select-sort-fields.score_desc"
    },
    {
      field: "dateAdded",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    }
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Hinzugefügt": {
      "field": "dateAdded",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Hinzugefügt",
      "operator": "AND",
      "showAggs": true,
      "order": 8,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Aktualisiert": {
      "field": "dateModified",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Aktualisiert",
      "operator": "AND",
      "showAggs": true,
      "order": 9,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,

    "Typ": {
      "field": "itemType.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Typ",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3,
      "help": "env.facetFields.autor_cat.help"
    },
    "Autor Zotero": {
      "field": "author2.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor Zotero",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "lehrveranstaltung": {
      "field": "lehrveranstaltung.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Lehrveranstaltung",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "Tags": {
      "field": "tags.tag.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Tags",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "help": "env.facetFields.tags.help"
    },
    "Zugriff": {
      "field": "access.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Zugriff",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
    },
    "Seiten": {
      "field": "numPages",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "label": "Seiten",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Priorität": {
      "field": "prio.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Relevanz",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
    },
    "language": {
      "field": "language.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 3,
      "size": 100,
      "expandAmount": 10,
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Semester / Lehrveranstaltung",
      "operator": "AND",
      "order": 0,
      "size": 100,
      "expandAmount": 100,
      "help": "env.facetFields.hierarchy_filter.help",
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "hierarchy_filter.keyword",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
      "env.facetFields.autor_cat.help": "Prinzipiell können für Bücher aus dem Katalog auch Daten herangezogen werden. " +
        "Autor scheint aber aus den Zotero-Daten bessere Ergebnisse zu liefern",
      "env.facetFields.tags.help": "Die Tags bzw. auch ein Grossteil der anderen Facetten-Daten stammen aus Zotero, " +
        "ob diese sinnvoll sind ...",
      "select-sort-hier.asc": "Semester",
      "select-sort-fields.date_desc": "Hinzugefügt (neueste)",
      "env.facetFields.hierarchy_filter.help": "Hier können Semester und Lehrveranstaltung ausgewählt werden. " +
        "Eventuell möglich, dass ihr die Hierarchie noch weiter anpasst, müsste getestet werden",
      "top.headerSettings.name": "Virtueller Semesterapparat Medizin",
      "top.headerSettings.name.Dev": "Virtueller Semesterapparat Medizin (Dev)",
      "top.headerSettings.name.Loc": "Virtueller Semesterapparat Medizin (Loc)",
      "top.headerSettings.name.Test": "Virtueller Semesterapparat Medizin (Test)",
      "top.headerSettings.betaBarContact.name": "UB Medizin",
      "top.headerSettings.betaBarContact.email": "info-med@unibas.ch",
    },
    "en": {
      "select-sort-hier.asc": "Semester",
      "select-sort-fields.date_desc": "Hinzugefügt (neueste)",
      "env.facetFields.hierarchy_filter.help": "Hier können Semester und Lehrveranstaltung ausgewählt werden.",
      "top.headerSettings.name": "Virtueller Semesterapparat Medizin",
      "top.headerSettings.name.Dev": "Virtueller Semesterapparat Medizin (Dev)",
      "top.headerSettings.name.Loc": "Virtueller Semesterapparat Medizin (Loc)",
      "top.headerSettings.name.Test": "Virtueller Semesterapparat Medizin (Test)",
      "top.headerSettings.betaBarContact.name": "UB Medizin",
      "top.headerSettings.betaBarContact.email": "info-med@unibas.ch",
    }
  },

};
