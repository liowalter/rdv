import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "basbibl";
import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/basbibl/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,
  documentViewerProxyUrl: undefined,

  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: prod.suggestSearchWordProxyUrl +  Proj + "/",
};
