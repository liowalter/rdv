import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {
  production: false,

  showSimpleSearch: true,
  showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "http://ub-lesesaal.ub.unibas.ch/",
  editable: true,

  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: true,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "sort_sign",
      order: SortOrder.ASC,
      display: "select-sort-fields.sort_sign"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Lesesaal-Systematik",
      "operator": "AND",
      "order": 1,
      "size": 100,
      "expandAmount": 100,
    },
    "type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliographie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
      "autocomplete_size": 3
    },
    "authors": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "localcode": {
      "field": "localcode.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sub-Place-Selection",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "dewey": {
      "field": "dewey.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "DDC",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "rvk": {
      "field": "rvk.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "RVK",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "sprache": {
      "field": "sprache.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "jahr": {
      "field": "jahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Erscheinungsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 10,
      "size": 31,
      "expandAmount": 31,
      "initiallyOpen": true
    } as HistogramFieldModel,
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [5, 10, 20, 50],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 10,
    "offset": 0,
    "sortField": "sort_sign",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {},

  i18n: {
    "de": {
     "select-sort-fields.sort_sign": "Signatur",
      "top.headerSettings.name": "Basler Bibliographie",
      "top.headerSettings.name.Dev": "Basler Bibliographie (Dev)",
      "top.headerSettings.name.Loc": "Basler Bibliographie (Loc)",
      "top.headerSettings.name.Test": "Basler Bibliographie (Test)",
      "top.headerSettings.betaBarContact.name": "Noah Regenass",
      "top.headerSettings.betaBarContact.email": "noah.regenass@unibas.ch",
    },
    "en": {
      "select-sort-fields.sort_sign": "Signatur",
      "top.headerSettings.name": "Basler Bibliographie",
      "top.headerSettings.name.Dev": "Basler Bibliographie (Dev)",
      "top.headerSettings.name.Loc": "Basler Bibliographie (Loc)",
      "top.headerSettings.name.Test": "Basler Bibliographie (Test)",
      "top.headerSettings.betaBarContact.name": "Dorothea Trottenberg",
      "top.headerSettings.betaBarContact.email": "dorothea.trottenberg@unibas.ch",
    }
  },

};
