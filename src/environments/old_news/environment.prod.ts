import {environment as dev} from "@env_temp/environment.type-dev";

const Proj = "old_news";
import {SettingsModel} from '@app/shared/models/settings.model';
import {environment as proj} from '@env/old_news/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,
  documentViewerProxyUrl: undefined,

  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
};
