import {environment as dev} from "@env_temp/environment.type-dev";

const Proj = "old_news-loc";
import {SettingsModel} from "@app/shared/models/settings.model";
import {environment as proj} from "@env/old_news/environment";
import {environment as loc} from "@env_temp/environment.type-loc";
import {addLocNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...loc,
  headerSettings: addLocNamePostfix(proj.headerSettings),
  documentViewerProxyUrl: undefined,

  proxyUrl : loc.proxyUrl +  Proj + "/",
  moreProxyUrl: loc.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: loc.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: loc.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: loc.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: loc.popupQueryProxyUrl +  Proj + "/",
};
